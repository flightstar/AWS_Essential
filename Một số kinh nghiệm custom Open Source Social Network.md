1. Đặt favicon cho OSSN:

Truy cập vào **/var/www/html/ossn/themes/goblue/plugins/default/theme/page**:

```
sudo su
nano page.php
<link rel="shortcut icon" href="image_url"> (thêm dưới thẻ meta)
```
Save và refesh lại

2. Replacing Main Page Background:

3. Changing Color Of TopBar and Hover:

4. Edit About Terms and Privacy:

5. Thay đổi footer trong trang Home:
   _Không thể thay đổi file core của OSSN nhưng css có thể thay đổi. Cho trùng màu với nền là đc: Truy cập vào **/var/www/html/ossn/themes/facebook_ossn/plugins/default/css/core/default.php** từ dòng 1703 đến 1737. Và đó cũng là file điều chỉnh css chính và cơ bản_

6. Hook system nằm trong file ossn/system/plugin/default
7. Custom đường link đến user của icon top-bar:

Vào file **/var/www/html/ossn/themes/facebook_ossn/plugins/default/theme/page/elements/topbar.php**, dưới thẻ:

```
<div class="topbar-userdata">
    ...
</div>
```

```
    <div class="topbar-userdata">
        <a href="http://fullstackcom.tk/u/<?php echo ossn_loggedin_user()->username ?>">
            <img src="<?php echo ossn_loggedin_user()->iconURL()->smaller;?>" />
            <span class="name"><?php echo ossn_loggedin_user()->fullname;?></span>
        </a>
        <span class="homelink"><a href="<?php echo ossn_site_url();?>home"><?php echo ossn_print('home');?></a></span>
    </div>

```