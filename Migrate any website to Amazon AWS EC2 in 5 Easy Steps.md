## Step 1:

+ Đóng gói toàn bộ website thành .zip trong cpanel hay local lưu thành wp-static.zip
+ Đóng gói toàn bộ DB Phpmyadmin có tên **abc** thành file **localhost.sql** lưu **.zip**  thành **wp-db.zip**
+ Chuyển lên host, đó có thể là github hay host nào đó để có thể sử dụng lệnh **wget** download về 

## Step 2: Tạo máy ảo EC2 và cài LAMP server trên EC2 Linux

## Step 3: Migrate File

+ Kết nối đến EC2 bằng Putty

+ cd /var/www/html

+ wget link_of_wp-static.zip

+ cấp quyền thư mục: 
  + sudo chown user_name /var/www/html
  + sudo chown ec2-user /var/www/html
  + sudo chown -R /var/www/html

## Step 4: Unzip file

+ unzip wp-static.zip
+ unzip wp-db.zip

## Step 5: Database import

+ import db into your mysql server 
+ change db path, username and path
 
  + `cd /var/www/html`
  + `mysql -u root -p`
  + `show databases`
  + create database correct with name old database: create database **abc**
  + `show databases`
  + `exit`
  + mysql -u root -p **abc** < localhost.sql