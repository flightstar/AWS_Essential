## Cài LAMP Server: https://github.com/flightstar/Install_Config_System_In_Linux
## Install Codiad:

This tutorial should demonstrate how to setup Codiad on an Ubuntu based system. Depending on your installation, some steps might not be required anymore.

+ HTTPD + PHP setup:

```
sudo apt-get install apache2
sudo apt-get install php5

Note: On Ubuntu 16.04 use:
sudo apt-get install php (installs php7.0)
sudo apt-get install php-zip php-mbstring

sudo service apache2 restart
```

+ GIT setup:

```
sudo apt-get install git
```

+ Install Codiad:

Note: On Ubuntu 13.04 or below the default path is **/var/www/**

```
sudo rm -rfv /var/www/html/*
sudo git clone https://github.com/Codiad/Codiad /var/www/html/
sudo touch /var/www/html/config.php
sudo chown www-data:www-data -R /var/www/html/
```

**rm -rf** will delete any folder, subfolder and file that you put after it, so if you put **rm -rf / or rm -rf** it will delete all files on your system ( or at least all of them you have permission to delete ), so be very careful with this command.

Open your installation with http://your-ip-adress/ and finish the installation.

Lưu ý: 
+ Nên cài các PM cần thiết trước: Nodejs, npm  với phiên bản latest,...
+ Muốn cài ngôn ngữ gì thì cài bộ SDK của nó hoặc cài trên trang chủ của nó rồi chạy bằng dòng lệnh. Bạn có thể cài mọi NNLT chỉ cần ngôn ngữ đó cài bằng terminal được trên Ubuntu. **Terminal** trong Ubuntu Server chạy được những lệnh gì thì **Terminal** trong **Codiad** chạy được cái đó nên bạn cài các chương trình lập trình gì trên ubuntu thì bên Trong **Codiad** chạy được thế đó. 
+ Mục tiêu chạy mọi NNLT trong Codiad có thể khả thi nếu các NNLt cài được bên trên Ubuntu
+ Viết Plugin, custom giao diện, phát triển mở rộng, viết đăng ký, đăng nhập cho user, liên kết với Openstack, các cloud computing khác, các dự án công ty anh em
