- Gói Free tier cho phép sử dụng miễn phí 30GB EC2

- Thông tin sử dụng dịch vụ miễn phí từ AWS: [https://aws.amazon.com/free/](https://aws.amazon.com/free/)

- [Tìm hiểu thế giới web từ con số 0 (Part III)- Amazon web services](https://viblo.asia/p/tim-hieu-the-gioi-web-tu-con-so-0-part-iii-amazon-web-services-Ljy5Vd39Zra)

- See list of key pair: [https://us-east-2.console.aws.amazon.com/ec2/v2/home?region=us-east-2#KeyPairs:sort=keyName](https://us-east-2.console.aws.amazon.com/ec2/v2/home?region=us-east-2#KeyPairs:sort=keyName)

- File key pair .pem only download once from AWS so you save .pem careful 

- Creating a Cluster: [https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create_cluster.html](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create_cluster.html)

- Danh sách các khu vực và vùng có sẵn: [Regions and Availability Zones](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.RegionsAndAvailabilityZones.html)


+ You can create and delete cluster

### Chứng chỉ AWS Solutions Architect
---------------------------
 Link: [https://techmaster.vn/posts/34376/ban-da-chuan-bi-cho-viec-thi-lay-chung-chi-aws-certified-solutions-architect-associate-level-nhu-the-nao](https://techmaster.vn/posts/34376/ban-da-chuan-bi-cho-viec-thi-lay-chung-chi-aws-certified-solutions-architect-associate-level-nhu-the-nao)

### Where’s My Secret Access Key?
-----------------------------------

Access to [security credentials page](https://console.aws.amazon.com/iam/home?#security_credential). Expand the **Access Keys** section, and then click **Create New Root Key**.

-------------------------

### How to replace a lost secret access key?
-------------------------------------------------

Follow these simple steps:

**Step 1**: Create a new access key, which includes a new secret access key.

+ To create a new secret access key for your root account, use the [security credentials page](https://console.aws.amazon.com/iam/home?#security_credential). Expand the **Access Keys** section, and then click **Create New Root Key**.
+ To create a new secret access key for an IAM user, open the [IAM console](https://console.aws.amazon.com/iam/home?region=us-east-1#home). Click **Users** in the **Details** pane, click the appropriate IAM user, and then click **Create Access Key** on the **Security Credentials** tab.
Note: If you already have the maximum of two access keys—active or inactive—you must delete one first before proceeding. If you need more than two root access keys, **IAM** users (each of whom can be assigned their own access keys) would probably better suit your requirements.

**Step 2**: Download the newly created credentials, when prompted to do so in the key creation wizard.

Bạn phải download **key** thì AWS mới bắt đầu kích hoạt sử dụng

**Step 3**: Make your unused access keys inactive in case you need to roll back, and then delete them when you’re sure that they’re no longer needed (after you have confirmed that they are not in use by any of your applications). The **access key last used feature** can help you validate if keys are still in use.

Note: Access keys in an “Inactive” state still count toward your maximum of two access keys at any given time.

----------------------------------------------
### What's the difference between a public IP and an elastic IP in AWS EC2?

+ Public IP get's changed everytime for an instance after stop/start.
+ Elastic IP:
    + It is kind of static IP for your Instance.
    + Doesn't change after stop/start
    
----------------------------------------------

### How to Fix “firewall-cmd: command not found” Error in RHEL/CentOS 7
```
sudo yum install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
sudo systemctl status firewalld
```
-----------------------------------------------

### Error connect to AWS Putty: Connection time out

![](https://lh3.googleusercontent.com/DC6xPUuXTmSNM841ex3KRvO7NTd7byh-RTIa4xtqyjXZSf7jywS17FzLQs8kRGtXci5V2iQNo8Cob-quxhcTSABwV7ZiDkW3lJq2ho-BkUAvzJ75Mi2yJmHlJ7lW2GJ59p3DS15DOubls65j3029VCRthImUD4kGPjVBOTKIOLd0pRV5XlXepqa4W6OwtGySDXNI4B7lZBYZiHG7hN7C48r3p7zrMTvQcCPu1Qg7dloeA3pvRgWeGlJJAf-P05qhRyAVdDzlcA0lb3LHXS_Az1tqZT4VlrQVNqukXDu1a0fGK7IF2zVzTwGPRseZGHczvwYGHZZO6HgnAKBB6PlzOy28qASXHNWEUp6dZ2AydvgI9EoYfDSG2_MxZdw80wJyi8qtvo0kkI0HDSdsoppYFJsA93fU5UAWBdL1C5HVRVt0oEXGgjmOMvwbiXBnK9CniSowLchG5rhUQWK_4pYFteR9zv3BRyUeEc0-eKtgti47SMx_IO7bogWUzS7O5lg02IZjOs8XIJgn_ogO4SWeuTo86vgBZx6T8hi4d54wx5u6Q5hJy47ev9CkvznI2mOTnpzBELiQq46Q2xP-MMyDrnJtJkj8uzHkJEj2_ZzP5yBCp2Rp4dKzODQGo6VFmtJFv3RJjsdCOgp3PDEJNNrgNXtmO19ythMM=w1072-h537-no)

![](https://lh3.googleusercontent.com/z1UgW4TKdi88pp7ss-8KBeyuTIpjLW1jU2UxA7-zrH649xyzdWvHTlagrB_TI-SmyMToYOpkcJii-jHqq7hXANxpb8I5eGk3ONxoUIPujqnBKmAfIzXu1QL84nHfehV4RaBkOdneqwNcLmBv-WE-o7C4CoerVFmjQ1q3Z46pus5ZNY8664B08IbWRLwDo3cmQy3T7SHBmVQYRiwTQIgUWgP5av08TusFleXZ_5Va6DBJ70vhbYg97yRb06gP8EUXLWQtQ3ZSah6dusUUX9UZ3aRO8_H7Bps2JHYRHYG03bi3lO6lP-cHrWliEkQU81exflJEkdgH8vPXXCFxP8XGh2aY_4_owPMmBZvezrZwo_02cTeGUwPgyJFBl9Lz6z_ZSpY5pYCm-bZy-RfruXiwC8IcgqwfTNb59HDSTLxkX30CbAjjnzzYHPB4Rxrq72_tUhp7eKuKfEd8yAZPlUybFxxvOUseHezma1Iwey67bC1GV8Xo2KRAuELR4yyXP9eBLYx1cJsPMr1wMvOu_sw3c-XPeI15U54USJmpCdamp8tr-nhZczxV3fQXszX5cLw2UOPCZDGOJwfrQMoxvWYJeEJKCdYemC3ZvoL05ofBbWIpfmLPtntYtuS7qTJZakl-ypNY4tdWSvlSUncpbm-jyu7jfwOwzvF1=w1147-h618-no)

![](https://lh3.googleusercontent.com/o5iEffIWEXASb8GtN9MWh7ZFLE7gdkL3YkcmoBjN5hwJsuT5cu6SEdW90EJnVt-6Sf7StzFrk5Dve2fO30hHbUdDFzak-BYkveA1fmy7P8Zc4NLbJEfq56UBVbcAiiVbz-Qrc9FRVpbpmhLooAqo4ijKK7DEHpQBYyyBA8dmojaxepbf9cnwiRFqLFh7dtskjq8i0Ayguu708DFoMuoDccSOQiZ_COPjX_NTJF2fePSxnZF8F0mwWYgkpR32xaMexlD71j6ccbMr2-6bl-HccJCj1gHyLRLX0jlsQDg1Ef8E6T1eniKUQNiXaAa1pWQc5fDKoQVGogjp9y8u5LtcsgeXsnYvscVJkMYipupTmhnmWNUleJfogju7OF5I_yPF39qWGj8sEFqYodAIqFtVy00cw27xBT8y42x3sNGLeamEd94T5KmKvKxdX7FTip_8bXKRzbPNW2OIfxsS-byL14QGlS87o1qgoYtmIoHsxskwMRDPl8xUVJKRk_1z5UTOXNnjal8SNKjVLQ-wqiM9_zV_nENEQFm9nHpvHA3KTK6jV0i436MKry_nZZAdu3engW-P9iI1IjRlyMTXCCiyGUvrOGQSfj7iokNU0uasw5o05R-axOssMO4BO2b-Gr1RlTKJGukFT1igF0ikfBleRPpEKoHH3-Cy=w1272-h621-no)

## [Connecting to Your Linux Instance Using SSH](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html)

Get the default user name for the AMI that you used to launch your instance
+ For an Amazon Linux AMI, the user name is ec2-user.
+ For a Centos AMI, the user name is centos.
+ For a Debian AMI, the user name is admin or root.
+ For a Fedora AMI, the user name is ec2-user or fedora.
+ For a RHEL AMI, the user name is ec2-user or root.
+ For a SUSE AMI, the user name is ec2-user or root.
+ For an Ubuntu AMI, the user name is ubuntu or root.
+ Otherwise, if ec2-user and root don't work, check with the AMI provider.

## Connect FTP Filezilla to AWS:
+ [Download](https://download.filezilla-project.org/client/FileZilla_3.32.0_win64-setup_bundled.exe) and install
+ Configure FTP Filezilla: [![Connect to Amazon EC2 Instance using Filezilla and SFTP from your local windows](https://blog.hostonnet.com/wp-content/uploads/2016/07/add-ssh-key.jpg)](https://youtu.be/JUqYie0lDjM)


## Github thao tác:

+ git init
+ git clone
+ git add *
+ git checkout -b new_branch
+ git commit -m ""
+ git push --set-upstream origin email_token
+ git clone -b you_branch your_repository 

VD: git clone -b email_token link_repository

## Mysql thao tác:

```
mysql -u root -p
show databases;
desc database_name;
show tables;
select * from mysql.user;
select host, user, password from mysql.user;
desc mysql.user;
alter table table_name add column colume_name varchar (20) AUTOINCREMENT; 

```


+ [The Complete Guide to “useradd” Command in Linux – 15 Practical Examples](https://www.tecmint.com/add-users-in-linux/)
+ [Delete / Remove a Directory Linux Command](https://www.cyberciti.biz/faq/delete-or-remove-a-directory-linux-command/)
+ [Configure Postfix to Send Mail Using Gmail and Google Apps on Debian or Ubuntu](https://linode.com/docs/email/postfix/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu/). Sau 15 phút thì PostFix mới gửi mail thành công.
+ [How to delete or remove a MySQL/MariaDB user account on Linux or Unix](https://www.cyberciti.biz/faq/how-to-delete-remove-user-account-in-mysql-mariadb/)
+ sudo: initctl: command not found: 
```
sudo apt-get install upstart
```

### [Install Cloud 9 on AWS](https://gitlab.com/flightstar/Cloud9SDK)
