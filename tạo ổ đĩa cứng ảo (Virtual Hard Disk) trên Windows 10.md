Về cơ bản, ổ đĩa cứng ảo (Virtual Hard Disk – VHD) là một định dạng file có chứa các cấu trúc “giống hoàn toàn” cấu trúc của một ổ đĩa cứng. Có thể hiểu đó là một đĩa cứng ảo nằm trên một hệ thống file gốc và được “đóng gói” trong một file duy nhất.

VHD được sử dụng để lưu trữ các hệ điều hành ảo, các chương trình liên quan và hoạt động giống như một ổ đĩa cứng thực sự. 

Cũng giống như Windows 7, trên Windows 10 bạn có thể dễ dàng tạo đĩa cứng ảo mà không cần cài đặt hoặc nhờ đến sự hỗ trợ của bất kì công cụ nào.

## Các bước để tạo ổ đĩa cứng ảo (Virtual Hard Disk) trên Windows 10

### Bước 1:

Mở **Administrative tools** trên máy tính Windows 10 của bạn bằng cách nhập **Administrative tools** vào khung Search trên Start Menu.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-1.png)

### Bước 2:

Lúc này trên màn hình xuất hiện cửa sổ Administrative tools. Tại đây ở khung bên phải bạn tìm và kích đúp chuột vào tùy chọn có tên là **Computer Management**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-2.png)

### Bước 3:

Tiếp theo mở rộng tùy chọn có tên là **Storage** ở khung bên trái. Lúc này bạn sẽ nhìn thấy tùy chọn có tên là **Disk Management**. Nhiệm vụ của bạn là kích chuột phải vào **Disk Management** và chọn **Create VHD**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-3.png)

### Bước 4:

Trên màn hình xuất hiện cửa sổ popup **Create and Attach VHD**. Tại đó bạn tìm khung text ở dưới tùy chọn Location, sau đó duyệt tìm đường dẫn mà bạn muốn lưu file VHD (ổ đĩa cứng ảo).

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-4.png)

Ngoài ra bạn còn có thể lựa chọn kích thước (size) cho file VHD bằng cách thay đổi giá trị nằm trong khung kế bên mục Virtual Hard disk size.

Kích thước của ổ cứng ảo được thiết lập là GB. Bạn có thể thay đổi là MB (megabytes) hoặc TB(terabytes) nếu cần.

### Bước 5:
Tại mục **Virtual hard disk type** bạn chọn tùy chọn **Dynamically expanding** rồi click chọn **OK**.

### Bước 6:
Sau khi thực hiện xong các bước trên, bạn sẽ nhìn thấy ổ cứng ảo được tạo ở khung bên phải cửa sổ. Kích chuột phải vào đó và chọn **Initialize** để tạo một Volume mới trên ổ cứng ảo.

### Bước 7:

Lúc này trên màn hình xuất hiện cửa sổ popup thông báo, bạn click chọn OK. Quay trở lại ổ cứng ảo mà bạn đã tạo, kích chuột phải vào đó và chọn **New Simple Volume**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-7.png)

Trên màn hình xuất hiện cửa sổ Simple Volume wizard, tại đây bạn click chọn **Next** để tiến hành chỉnh sửa các thiết lập.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-7-1.png)

### Bước 8:

Lựa chọn kích thước (size) cho Volume mà bạn muốn, sau đó click chọn **Next**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-8.png)

### Bước 9:

Trên cửa sổ tiếp theo, chọn tên ký tự ổ đĩa mà bạn có thể đặt tên cho Volume. Sau khi hoàn tất, click chọn **Next**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-9.png)
### Bước 10:

Trên các cửa sổ tiếp theo bạn click chọn **Next** cho đến khi xuất hiện cửa sổ Simple Volume wizard cuối cùng, bạn click chọn **Finish**.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-10.png)

### Bước 11:

Lúc này bạn sẽ nhìn thấy một ổ đĩa cứng ảo mới xuất hiện trên cửa sổ File Explorer. Như vậy là bạn đã hoàn tất quá trình tạo ổ đĩa cứng ảo.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-11.png)

### Bước 12:

Bất cứ khi nào bạn muốn unmount ổ đĩa hoặc muốn lấy lại không gian của bộ nhớ, chỉ cần kích chuột phải vào ổ đĩa cứng ảo mà bạn đã tạo rồi chọn **Eject** là xong.

![](https://i.quantrimang.com/photos/image/2016/08/19/o-cung-ao-12.png)