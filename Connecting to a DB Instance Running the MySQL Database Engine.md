[AWS Documentation](http://aws.amazon.com/documentation) » [Amazon Relational Database Service (RDS)](http://aws.amazon.com/documentation/rds) » [User Guide](index.html) » [MySQL on Amazon RDS](CHAP_MySQL.html) » Connecting to a DB Instance Running the MySQL Database Engine

Connecting to a DB Instance Running the MySQL Database Engine
=============================================================

Before you can connect to a DB instance running the MySQL database engine, you must create a DB instance. For information, see [Creating a DB Instance Running the MySQL Database Engine](USER_CreateInstance.html). Once Amazon RDS provisions your DB instance, you can use any standard MySQL client application or utility to connect to the instance. In the connection string, you specify the DNS address from the DB instance endpoint as the host parameter, and specify the port number from the DB instance endpoint as the port parameter.

To authenticate to your RDS DB instance, you can use one of the authentication methods for MySQL and IAM database authentication.

*   To learn how to authenticate to MySQL using one of the authentication methods for MySQL, see [Authentication Method](https://dev.mysql.com/doc/internals/en/authentication-method.html) in the MySQL documentation.
    
*   To learn how to authenticate to MySQL using IAM database authentication, see [IAM Database Authentication for MySQL and Amazon Aurora](UsingWithRDS.IAMDBAuth.html).
    

You can use the AWS Management Console, the AWS CLI [describe-db-instances](http://docs.aws.amazon.com/cli/latest/reference/rds/describe-db-instances.html) command, or the Amazon RDS API [DescribeDBInstances](http://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_DescribeDBInstances.html) action to list the details of an Amazon RDS DB instance, including its endpoint.

To find the endpoint for a MySQL DB instance in the AWS Management Console:

1.  Open the RDS console and then choose **Instances** to display a list of your DB instances.
    
2.  Choose the MySQL DB instance and choose **See details** from **Instance actions** to display the details for the DB instance.
    
3.  Scroll to the **Connect** section and copy the endpoint. Also, note the port number. You need both the endpoint and the port number to connect to the DB instance.
    
    ![Connect to a MySQL DB instance](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/images/MySQLConnect1.png)
    

If an endpoint value is `mysql–instance1.123456789012.us-east-1.rds.amazonaws.com` and the port value is `3306`, then you would specify the following values in a MySQL connection string:

*   For host or host name, specify `mysql–instance1.123456789012.us-east-1.rds.amazonaws.com`
    
*   For port, specify `3306`
    

You can connect to an Amazon RDS MySQL DB instance by using tools like the MySQL command line utility. For more information on using the MySQL utility, go to [mysql - The MySQL Command Line Tool](http://dev.mysql.com/doc/refman/5.6/en/mysql.html) in the MySQL documentation. One GUI-based application you can use to connect is MySQL Workbench. For more information, go to the [Download MySQL Workbench](http://dev.mysql.com/downloads/workbench/) page.

Two common causes of connection failures to a new DB instance are:

*   The DB instance was created using a security group that does not authorize connections from the device or Amazon EC2 instance where the MySQL application or utility is running. If the DB instance was created in a VPC, it must have a VPC security group that authorizes the connections. If the DB instance was created outside of a VPC, it must have a DB security group that authorizes the connections.
    
*   The DB instance was created using the default port of 3306, and your company has firewall rules blocking connections to that port from devices in your company network. To fix this failure, recreate the instance with a different port.
    

You can use SSL encryption on connections to an Amazon RDS MySQL DB instance. For information, see [Using SSL with a MySQL DB Instance](CHAP_MySQL.html#MySQL.Concepts.SSLSupport). If you are using IAM database authentication, you must use an SSL connection. For information, see [IAM Database Authentication for MySQL and Amazon Aurora](UsingWithRDS.IAMDBAuth.html).

For information on connecting to an Amazon Aurora DB cluster, see [Connecting to an Amazon Aurora DB Cluster](Aurora.Connecting.html).

For information on connecting to a MariaDB DB instance, see [Connecting to a DB Instance Running the MariaDB Database Engine](USER_ConnectToMariaDBInstance.html).

Connecting from the MySQL Utility
---------------------------------

To connect to a DB instance using the MySQL utility, type the following command at a command prompt to connect to a DB instance using the MySQL utility. For the -h parameter, substitute the DNS name (endpoint) for your DB instance. For the -P parameter, substitute the port for your DB instance. Enter the master user password when prompted.

``mysql -h _`mysql–instance1.123456789012.us-east-1.rds.amazonaws.com`_ -P 3306 -u _`mymasteruser`_ -p``

You will see output similar to the following.
    
    `Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 350
    Server version: 5.6.27-log MySQL Community Server (GPL)
    
    Type 'help;' or '\h' for help. Type '\c' to clear the buffer.
    
    mysql>`

Connecting with SSL
-------------------

Amazon RDS creates an SSL certificate for your DB instance when the instance is created. If you enable SSL certificate verification, then the SSL certificate includes the DB instance endpoint as the Common Name (CN) for the SSL certificate to guard against spoofing attacks. To connect to your DB instance using SSL, you can use native password authentication or IAM database authentication. To connect to your DB instance using IAM database authentication, see [IAM Database Authentication for MySQL and Amazon Aurora](UsingWithRDS.IAMDBAuth.html). To connect to your DB instance using native password authentication, you can follow these steps:

**To connect to a DB instance with SSL using the MySQL utility**

1.  A root certificate that works for all regions can be downloaded [here](https://s3.amazonaws.com/rds-downloads/rds-ca-2015-root.pem).
    
2.  Type the following command at a command prompt to connect to a DB instance with SSL using the MySQL utility. For the -h parameter, substitute the DNS name for your DB instance. For the --ssl-ca parameter, substitute the SSL certificate file name as appropriate.
    
    ``mysql -h _`mysql–instance1.123456789012.us-east-1.rds.amazonaws.com`_ --ssl-ca=_`rds-ca-2015-root.pem`_``
    
3.  You can require that the SSL connection verifies the DB instance endpoint against the endpoint in the SSL certificate.
    
    For MySQL 5.7 and later:
    
    ``mysql -h _`mysql–instance1.123456789012.us-east-1.rds.amazonaws.com`_ --ssl-ca=_`rds-ca-2015-root.pem`_ --ssl-mode=VERIFY_IDENTITY``
    
    For MySQL 5.6 and earlier:
    
    ``mysql -h _`mysql–instance1.123456789012.us-east-1.rds.amazonaws.com`_ --ssl-ca=_`rds-ca-2015-root.pem`_ --ssl-verify-server-cert``
    
4.  Enter the master user password when prompted.
    

    You will see output similar to the following.
    
    `Welcome to the MySQL monitor.  Commands end with ; or \g.
    Your MySQL connection id is 350
    Server version: 5.6.27-log MySQL Community Server (GPL)
    
    Type 'help;' or '\h' for help. Type '\c' to clear the buffer.
    
    mysql>`

Maximum MySQL connections
-------------------------

The maximum number of connections allowed to an Amazon RDS MySQL DB instance is based on the amount of memory available for the DB instance class of the DB instance. A DB instance class with more memory available will result in a larger amount of connections available. For more information on DB instance classes, see [DB Instance Class](Concepts.DBInstanceClass.html).

The connection limit for a DB instance is set by default to the maximum for the DB instance class for the DB instance. You can limit the number of concurrent connections to any value up to the maximum number of connections allowed using the `max_connections` parameter in the parameter group for the DB instance. For more information, see [Working with DB Parameter Groups](USER_WorkingWithParamGroups.html).

You can retrieve the maximum number of connections allowed for an Amazon RDS MySQL DB instance by executing the following query on your DB instance:

    `SELECT @@max_connections;`

You can retrieve the number of active connections to an Amazon RDS MySQL DB instance by executing the following query on your DB instance:

    ``SHOW STATUS WHERE `variable_name` = 'Threads_connected';``


Note: If test workbench don't connect to Amazon RDS, you can fix:

Click to **instance** -> choose **DB instance** -> At **Cloud Watch** search keyword **ip**, it will show:

Endpoint | Port | Publicly accessible
---------|------|-----------------------
wordpress123.cye7oskbf4om.us-east-2.rds.amazonaws.com|3306|No

Change **Publicly accessible** from **No** to **Yes**

Click **instance** -> choose **instance option**

![](https://lh3.googleusercontent.com/IJxWJA0PfHsNLXZUejZHS149myfaah6Td-wANVl8iiq0QViEiqy074WwPeGQQn-vgiy-ZF1o4U14y4r5KTgexXfAf-iqo5m0ubomuNoyRpUH_gytZyIIj_bG5qUR_WKEGlKuqBrfpZgxAFuN2s6BqpQfS7mObGUQw8xNcP1EN3QBrQ_8QSThPLyJpiXxC6MD1xMf1Td4JH94-eBP_ibvlNSlR1I7D2PURhyyCxCdMhBA2spZqcZys_ZuwS-u7AALghSUdoH_n8HigmsHj9rwtk7gUTgUwFdjP9UgawAnG3kYCDB1ihHHnOiWmQERW6F0ifrihkpX2rciDEtplZeThsAmS6H5BFaR7CVCiwx6UpX3A2e9qw0DMhnE7wantoEMSvkIWSwOaxQWbNy_NRPn_CmuV6Af3ZNNuYzJpKImrqfluBujrx1aa2wtS_oN7rKxJy4jiuMg5rgsopztPX9OBQA8uhHjeq7eDcMoVa_gDEGOzTjX6OGFVVUnleuJ1yruk3RwcbQL-sNPnHbXOVqSV4je0URmQZ7hp_rvYm_PFBODT5i6H7XoGnZvXWDJycoOXm7jlWQrwRhyKwKeaNJ5-q_dB8kIpzPoSFHFHXl2gKK9qMOvb3Dz8QGsRs66ad_tE7QYNauDQNjsEXkt0_NmuXhjBJFmPip2=w1266-h604-no)

Choose **modify**
![](https://lh3.googleusercontent.com/T_l5Lq-CqRRRjm5kTCaHZM55cHBJGW3XiJNeChhy7DBrPtceKwRGCINIg0N_7WUzU7QNAo2RGtVx_wilHIhtAI4TMcZJMVCosKk7zuo6nAnt0JLBdKg9KU7yoomPehHDssWkDZ0UjrCK7NcloxyoJoSXpiqMmc4rDB4MdnDosUGuqM9nyaLzWwjPqvosrEwS-OVyDg8PpbQr_isxZudGOBgxsDBX0XHkYk7prxLnpLcvmFWLFITISk1-l-kVaoxAiqLnL3hyHVeCEc7RO0jwaCJjlEQ3rYSkadGmYA4PddAIvSASLEq93xpyL_kaAzGR3Zfam371608EZzYu0DV-D-h9qkSsdpIprDyovV8nifQEbK4DCIRDhG2PZdmrgLqKJ1vqgJ_CFDHYmgrXvbHZonndCkvQGSS07iC7RnbPRdCBzAvs3j6yucDIggK05zRlZ9g7_I7Wgx7YZUsx-aGFpBsBrr1bfSKTyycF24oTQsIf-Q17qT3oAuz5MTyUv_xe_l4RgOPknDnXQhTKSEg4_1tnvzxaoCPPxUHfZO4nz4TF2KgJrrFPiUCE1fn8VWm-8LBqBMwG9zU0TolLiOvltEGOaB2Z6fdeBCHkTMR2xj_hgTFGF5rGjsMUFAFapbqk1VtUcOLf8fj5xDH5pM4KmBBJn9C6wRIL=w1249-h587-no)

Change **Publicly accessible** from **No** to **Yes**
![](https://lh3.googleusercontent.com/gkEK_pyx0CLtTmGx84bff3QUysvtMmm6xZH7VIcXjyztgOZNqm3o7I0i5nhlm_hGdntkUhTl9H5J_ElgIY7AXKMNb5RGb5YZtAEQMwaNd95TzISlul9nFNrUUhhMV6dkST-5SVNdRWjIQoeH8WJ92K8UrWd3UgM70EwrvfYBVrZ9lWWmmHD_X8o1K_iBt_iviGL3u7FeRMGAezBR5eWy8Jv8Xi2jKM9lH4xc2Idlrqi6tQQ70q2vsQSgUzoEivrQpf6drqE9ZuemrnSf8BbckykyfUtzufDGCTqD0us3MwxknTPsXGB4_GoavkjBeyNKddaQIHsIAXXMWuJ28sJ9x8ydKxa6M8YiDJlocmgggwrqYwODhzJKz1BWjnsp-LnCWc5CSIcMNFsWdTe2_9imPXJxOXAc_iOF5CNNH4FCScjY7URxx1AcT2-fDFgjgSS9iSk3E5BWVU7ux9ehvuDbvQ_gMzmogXQs8QVwhLEr9yJ0L4I9PNwUzgVW490oxwsMsnGkHqWHkXhZiq-J1MBCaaosJNe51dWs2dxzSGEGgcOyaoOsMfRb7D46EVxqnycwzi9dXkINFyhSofJzUFu-8jBqs6kiHNB1zjkJFGGL3ed0F4IWC6Qi-rUjcc7tCXsq81zueyMhDVviW3NRt4X5SONQUDvPqaEY=w1237-h672-no)

Click **continue**
![](https://lh3.googleusercontent.com/sA24lX3ep8tdcEjGK5lUmY8M7eqTHlKcnsH5E9B-upGiasOxRl_5WHjrP7VAqm-Cx-bKlNKCSq6imA3wlbTK3Gs2JOAE9sk25-fZnTtNPT2OOc9XH2Vz5B7T5ULBQY3X6xSP4r2i7ZqCP8fSaVPAoY1a2Lczb1LgFffPtl-6XTIUNSfBuBbt9e-2L0y3-2UlmpTf-2ExSlaTunsuX6c1DryXPHqImWCbrzfpMms5q3vMWTevlszl-_k6D_xDQDjIykVAZCXdM-U13eihRMTQPsJoe-3ul_evNkPNCRrwgskmPSNSkgIzjcqFcl2VmZMN430uh-yLFrEhBk0xMPTzWksN2q8Z1oPp4tXJGybi1Kfi6FwMS6fpUF6GjPVyN95fllEJpoTdQ3fXC8NoLkObG8XJvOhbIYqqPhibEAecXsE-QD8o00ziUoJ_DGygX_SU6Uvj5Y2Sjeh_SRUolgRSZ5tzeHwsGUp6NxwzEU3FxELRl2p8oB9v4bbXgxNb8oMzt8AqViaUkPKXmNXLMapiSaMnLNnYhTloCnROpQA9wfxR8ZBSzztzA_4qWyDAzZqG-N4BFBi2ETfPm4eU_Ozp3X5sOJUW3Sw7XwdHI6L5_ksvkeSHi_Ui5T8zlp6ZgJM8MnWSPfDBhqk64bLJzjl0Sa4B461t-YFW=w1231-h662-no)

Wait for 10-15 minute to **modify** instances
Related Topics
--------------

*   [Amazon RDS DB Instances](Overview.DBInstance.html)
    
*   [Creating a DB Instance Running the MySQL Database Engine](USER_CreateInstance.html)
    
*   [Amazon RDS Security Groups](Overview.RDSSecurityGroups.html)
    
*   [Deleting a DB Instance](USER_DeleteInstance.html)
    
*   [IAM Database Authentication for MySQL and Amazon Aurora](UsingWithRDS.IAMDBAuth.html)