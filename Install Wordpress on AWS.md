We can't delete **instance** in AWS: [https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/terminating-instances.html)

[How do I ensure that I am using the correct user name and private key when I attempt to connect to an Amazon EC2 Linux instance using SSH?](https://aws.amazon.com/premiumsupport/knowledge-center/linux-credentials-error/)

[Connecting to Your Linux Instance from Windows Using PuTTY](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/putty.html?icmpid=docs_ec2_console)

[Troubleshooting Connecting to Your Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/TroubleshootingInstancesConnecting.html#TroubleshootingInstancesConnectingMindTerm)

[Oracle Trainning](http://www.appsstuffs.com/courses/)
[Oracle Cloud](https://cloud.oracle.com/home)

## Create VM **instance**

### Step 1:
![](https://lh3.googleusercontent.com/fidZfioqXnnsw7j_huE4ihluQzA3E99CjgH8TL9Ece_kw9UySH6NNOA-fDv7Mv-JFLRFcZCBE8RbOqFLhV_RUKipTvTouFY2GeI_HuABKgQoFFcDCr87-yAk8pG4-Mlto5ffJXgcjuqKcP2FaSDgBJUNT8RYGw8_Si1wPudsNNPI4hmiS_Fmuxe6e8UkY2s0n1oK3clgAduzzATVICRw0-tTAm12huRmJSloQiPY4BXtspUzvSncj_ddkhE9_SeaCsHTC4sY5a3eQcW6IIcQeBZKHhGxfv4LLPAIaKG8yVdUzjEpSPSnhftFgi-vuNIDSyKZJhkgaosxC9dxQ8KD4_hnpB-xxs6BF15SK436pG3COP5YoNbJzReg5eBg2ZJzJbgA_o-ilSiPtnzcKIEzOL4MVTJ1wU1ST0kd-yTthWUsrszTpDL2iPPJaGTx-lKyQvTHdpm_RSwAHIVVQ0rzy9_EkIswYBRHhskYGHFxoWm4EGtXtl6z9FhFWRDNIG9Bf3hDIBCeP2bxZxG9VAWWhLAmYXG7_aJAT7b52zCbzlz3MIx5NIzKP00JdBT9KrP6bGeQ_b4XRezI6At1XfBPw_NiTnYYb_AGGR_GE2Pu2fcPI64zVqmlR8CUU9z_CCFyjrL38us1AQrcL5DmTPmscS2Zyo9JCBDT=w1086-h354-no)

### Step 2:
![](https://lh3.googleusercontent.com/lp-2pbFkrOn68qTLcILfgJ7kbRMarrGyxBBIYMINiAfyUGl7I_ZDIuGQ7S2_gyFMuglhp5qhNrocUGOXjUJCEYd8T-lbEQU73yE4rCUqkBCdJ0G3N8nW2GVG4EFN_QwlmUFflVw3mdADLw717UGTEqlaAIzkxZ-TAC38cYluotuSRP0iHG7EMOExBW5muWEi16iv2zsm2YXB-fXb1mGubrnJZB-_K2pag4BR2aX6wPVfj73Qx-CIkbvO8HhqjWE4r4O4LT8r6A88QSMXP6S_VOAySq_AlkZvUpiGR-rJ_osm67FwNTYm987XgRQiY38k2kvWUBeacwd7BuCfRTHO5j7ZeGtvSjY0uspRcuphY1J-lTXc9HpQw5k5Q_gurHwn4b-GFVmlUTxnSaX5ib260SFvI1zsZenHDB0cDTr19iGYHRHJOcjK3ttozMzP7vk2TaKw97jMkVCNg4SztnQfkEPgp4G5OyxFdOwuLV18kE-RPirzl5dajZEYckxme9V14gnuHjsYHeCuvDxWv5o9yBgn0rt3Hwts1APBFf2nsNCB2xhPi2GZmYaUR3ubetBQpTPQ9yXz6cMXvIHEy3SqZPVh1r8sdjdA0PUHGdGrPXIYHyWliJ_uJq5H5iE8sIaNr3FuT2e8mTsfLZKCJ-VNPUjp5O1TNkjm=w1266-h593-no)

### Step 3:
![](https://lh3.googleusercontent.com/IvQk2IQKTJj80X0RNkKIDguhw5l46F_dRvshtQX2lQOx2qGeK4LQkF3OKskDobiAjJ3M4CAWdc3OKA9kmJc0wz8tK_ykCr4v0Q2K7iMePgArEaRp4-AY8fd4srn9MMs6omOOEVKttxnnLBSxDEu4wYrClKbH-vMYno4VjIyxWHqR0aBAhKsHKTfMFkeD8MNWkVJE-vLXKzFQSInFVN79aEEKpDihlMXMU0sJQqFkhBSVUVjKKX7u0eUGTOGJl38QMA9OlFfk7UkscI7Na0SyAqtP2Z7jixNGaq-lgYgmHgBrvZ5xOU3mgLYtfI1rXYzpkJiZYGxlP_TxmWf3Qx9Hy1kjDzrLEBp79qBEwfOh2IS_7u4JDFhlW9xjc-xv-MXKHSA82ByXegIPVbdvz2qqrZrseIzB8cqcsWIanmT4UqbhdkLKJaOtXqU3pL3o6BLsG-kSB5Glyz2ofo3D05COD_FTu3utr11SnwkorKs3PYsfP5jC8_P_EosTU7kOW0wDqS4Gmq5vQOLrE0_ISRwLG3_RE1xrbn2nJDrnWXSzdurTJPPeey07anentF6GsuqJqX-bJlFpdQipTC5k6bM4ug1UWiT-QBWgLSpLOt3As29RPznbcz01amlLPHy-QYxr_ac7p6g4Va-_30nfj9bEdDyJUZbxN2F7=w1280-h620-no)

### Step 4:
![](https://lh3.googleusercontent.com/3hvt5kJVEBh9pbep_ucQjNzA6Q9ldktC-Isnx1vrN84dlAZBbdGRJ19pIjMo1pULCyTxbelDYOY9By3QnDetvHBMVjfLoC6iyFxVUnpS5cQwf0Wtt8mnaoA4uDoeR-cg5sXzbFpNQ5PKRDZYm3toj3k9Ms89UpWaD_62vTQ0lxUyVdMx-07Xbsop1HDpmQBVhgjrto1TCcH1BJ_6g9PuzzrcSXakrjvU58hhvp_ZfrHjoBc6Gume7PAaHISZTkocZgQwoZw2cPFrvJKO2hNPsylEGl8ClFAe4dD-QDDrU9pkahcaqiLYw-Jq_5rqvbxSZKynlGq3AwBYJxLdsN0qD3PAz74uIno0_POBCiavw5t2QzPORQvBlmp-DfmW7S13zF1Xl74LPL2hPhQz7xBsm7QzAgWSLnZCYAwqc0MerRMxp0wAtzJ9SrBYo0sDkyYiM9yF71_DhlAe15aL1Gfl6QeO4E9CXj5d2Wj9ll_G6f6BGJi-JMaPWXFwu9W_FfSNJH1x6GBIn1XCWfbu46SgqKKPsP7hYOWG3oco_qca-5Fs_QLBVvW5WnLioDSKx691RhoIGB4FZv7Zz5I_n0awkVZxzw68xjpkgeVk6xLIY9K8Xc0q6zB4AI4zPQbY_mZrQeT7vYEYI7QB1qzhBfeCsknVCdGPBJLP=w1277-h616-no)

### Step 5:
![](https://lh3.googleusercontent.com/-QBYjbMl0bpoZxSiN7bFmFrsUsaYhEGLS4Ay0nNFEfCCKGcxWcvU3cbdVvg6yG_WL6KDh4UeQbTaL9WUyq7zKpVD_xOm4AcwX7GbrSgHMiJMUtL-bxM0qJVzb6PUFRf3JpX9N4pnDBtSC0f4AjSqEbfNY12302t7fogksFnO3lsEXKJfrNT-oS3sMBB6-44q5e0xmGSMtr_Q0OZrqSoP35Vsq2MKUUWBvbKnvHuzLvLi4suFLxmOC7GNvnmEU2jyrWOY2epC7MLzE_wtXe4FMgXEgTCUQi7e6QeFgpElaoL2g-EjvNQAB4deiN53k8DQwMmDYkxhEsNE8dZXA7kTZwWbxbN-8CWukHPPotB31Mb3iN1FkjidHMEXjoUGj4_e-jD9i9YD4zglJn2SlmKwclJDOdwqcxkSmKGiZkykqC-urWA2B7qezuNfhUhlNtcYnyNckSTewLc38TrOkuyJw3lBcrUMrYpYWhYHHkCcds4h5W8FaodbCBqrrjtLpwHlgT1e9YNUuk9ws0KHgd2QvyDObq7OoJqwawnND0BsmhYX1WzByyXKFbQ_bcN9okHgC6kWZSNHAL5o4J5PKfYWqFyxYF1DAEbkVj_mw7MXmmCfIGmR-dN1DiSfYTB8Gt71nRjwtwPOgLKT9xsekq4aDWgPXALLhXvY=w1274-h622-no)

### Step 6:
![](https://lh3.googleusercontent.com/dbjZvZHCVTi2RCIKpD4qurbRHMYjh7IO6ndunEetVsjmlOoQ_LqI450XPr3UxBmnSISCQadSTGrbS4bvzQxkKEoAW1u2WdQafluOKXpANvSrzLANF8xXqC60NKXCbOUTku4nTyDAO96XPQwIYUnn7Rbek5VPnxvKND3SlMlfgbjBe-z80M-TMLnlVoatxJ1v6zSpDLSQlYEx_Gz822qqOYJjOneq_sYR7ptr8vqdls3RV9Z6sjMEh9fcb4Dw0KMJ0sBk8vTI_4qUplXx5IP9TA-XkxVPoCQQ3a7SpRm6izhV35DkUmnNtzs98f2JoLu2yDMq5BUFLuvdX33TugrsKD2Zx4-mTzRgixUYxpOuP9vqV-KWlUw5kWEuIOaxglPdTo4Z2b-dfz_pqQhkpAx53Jn85_Ena5hcOQoQTR4sqRag8anf0KtM7vkxHZio9eFJI5M_tRjiOPXfUm9PrbKMBtElx3bEbrCiOVh7p7ulxGlg0Xeh6Jutukjn8o4hoiDk_y14aET7Jv6S_14HibqmDkd4s7JVZiGVsO2Cc1hO1xzqMavlC-Gt8dwktoz1DL2upURrv4Mso8dGrlTMQKBvZSrmG-LSfKSOEzkcvomMZ_Z-Tu9J-cWW2NnOpNscYS_jAkKkph4wBUW4eThybF0efepsiukPg7f8=w1284-h617-no)

### Step 7:
![](https://lh3.googleusercontent.com/d5dadRydQEfnMCM8msOgKYj4JM_yuAGcyzUVKp3_FdiH1Uz2cKE_NuL1vFCrG6G3bt_ws0lyAxQLEYfwR49UaNeDquk-B27cV06uM3zTwt2rNBosyYk7FAulz0Xb_Ti_z3YbL3kduPwu0NVeLXt6RobxTIRIgJVPC3ZDqdkuLV5t4FNR-4bbiAzN70ilkpjH3SNFPPkCb2O4wUf6TR_Oyeg59NBiiZoL_6F-_dnfdHR1YTxrBaeKKcAgYDvECv2ZrIWTyeGNUgu7W0N94H-FZO0ofelu9e1LEEm6NdZa313vPAaMlNUJz87EF2ESbr_k2ewNOERNhPEUK7n3AMk9Jru-YPv06CwEhIaK9PdrTDFTG9rGIZYap2qRvGshbPcWWcAdwsGej95gI_IzRp3K6cQbYeUdx9l028PvycwXNfI4q-SICMRIvkkl1YTmW3xIkmfeB4O5xLrc_fvc_hTDJM-ZsMbSIsUQLfWKf6AGMir5pXlB7VzH6opNefOKyJb_ancpI_OJRewYA3_kJcE7AWd23st32JElQWC6JQneV3tDnxAce1rx0ieAh3LyqTzOQil5EjMX8BHW6tIJyI8M0GkERP8MfHOe1muCTKrgR7QLy7oiH-SJgmiYUiVWhBrM8kpqRtxrBfsbsjKYCjGd-m-UI9MAKrYd=w1283-h623-no)

## Connect Windows Putty to VM Instance AWS: 
+ Converting Your Private Key Using PuTTYgen
+ Starting a PuTTY Session


#### Convert Your Private Key Using PuTTYgen:

+ Start PuTTYgen (for example, from the Start menu, choose **All Programs > PuTTY > PuTTYgen**).

+ Under **Type of key to generate**, choose **RSA**.

    ![](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/images/puttygen-key-type.png)

							RSA key in PuTTYgen
						
If you're using an older version of PuTTYgen, choose **SSH-2 RSA**.


+ Choose Load. By default, PuTTYgen displays only files with the extension .ppk. To locate your .pem file, select the option to display files of all types.


![](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/images/puttygen-load-key.png)

							Select all file types
						
+ Select your **.pem** file for the key pair that you specified when you launched your instance, and then choose **Open**. Choose **OK** to dismiss the confirmation dialog box.

+ Choose **Save private key** to save the key in the format that PuTTY can use. PuTTYgen displays a warning about saving the key without a passphrase. Choose **Yes**.

**Note**

```
A passphrase on a private key is an extra layer of protection, so even if your private key is discovered, it can't be used without the passphrase. The downside to using a passphrase is that it makes automation harder because human intervention is needed to log on to an instance, or copy files to an instance.
```


+ Specify the same name for the key that you used for the key pair (for example, **my-key-pair**). PuTTY automatically adds the .ppk file extension.

Your private key is now in the correct format for use with **PuTTY**. You can now connect to your instance using **PuTTY's SSH** client.


#### Starting a PuTTY Session

+ First, You should create **Elastic IPs**:

![](https://lh3.googleusercontent.com/CpX4_DbcJmcSgzcyEO0zsIv_ZtiMgwUMPSqsHd936ZnG1u-A7JnA6-byCq0yqqCZBFXpwdkHS2LMJpDP78nJcDQ1yLgPLxbmIhaJvGiLs5OBnh8qDsr2m2slDjOu-GBjj4ymX3whi7H6IxBgcdqKthu0i90hKZNcgDuAW90h9Ya54ghQWAmIyMD5A1n_A3PlhoVacG4kHmjc9F1pZIofjbYbtO1HO33dBiBc1VHTF-H9Fkv8Qsjs6wULsiQKYBPr_VBdyetVpLSgZwJDK76Ix2a0yHrgAq0f2LuKGsX0CUDAxxbojrU2pa8a3P8Xai_7wKPzu3ibVR4T3_wEh4Wuuh6X_L3FcN5-CORkOFMAK18rgOzfgfNniQgg7rYpqgMkk5UCDL16KXpcotGgWQjPYYeGqRf3Gu4P0wK1Sg3jvHODNRxx3SK8Hk0sEkhOQgvoXqnNUe9vzkWNlJA0oUYW7RZx8M_ZjhTSuP-JIS1Ehm-H597zLAlT8dlcxUOtaqeSGPEGaHEytY2sXCqWKNdRt1RzgsvpV7zNg8GR1VZ-FcpxwNNinVEzeEgHVpDnxnANxTN2kyVHmZ9AXWFKdiYFnoo0LiZ7twwXWcQoJxM_pG-I9is3zyQCnaM8SDo9ilqnn3HhTeF4o1_PvLrOZpVRmRArs-eJX3-N=w1287-h507-no)

![](https://lh3.googleusercontent.com/mbIE4OgXhyX1_gW2p0sZExEgMAdUG7omjAw1XbfNoaCaztBJZbmgvPyhHYAq9U2xpvXFqs3VdgEyDW-6E6lTPGlQrlcPe6mrdKdhwHCBWLqsELEL8fNA4tUEXBsl2i932Oqsy7djLK1yg1Dvm5HgLPXTO6rrGE3Mqnnk3r7rhVrQkxW2fjB3PBGZqdDytUQxUzp6R50_XTynWt5mvzZiggz9vsMYxzX_nzz1_iIMiLkYxBpunhmbRmEgai564QK7hpLaRUkpQctuqUIXQVkZcZzoeMcMBCM8-krXPG4E2CS8r7iqk9VvaZ6Vfs6tXX7R1NqTSu1zpis63sVAsRZnNvo_dz_9rBkzpbi8aF9AMdeiOH1om6hRhAmotVx1jYo9FINKHwGYDPVGeeIT1RSSA__ShYYAh4pRk3MMSohNgamOBc6UFfafnpO8-VYEscyfbGGASypxKT5etGTa8_SDvmmpfV2LZC9QshxqmJQUQ4G0KRXRpSXjahPOEDrvCE2mLeWRi8B4zmslBh2v3IIEe85keNFEsV3k9NgAvk52ksOLc3HpAzs13l9KravKErXkqOSeqkZo0TkcNm2phS2b129xglsrbifI0q_d2x1pCYjCj8tOn6bKmLJt7y6tHF3bTi7MpBxJugmoYntP8YERoyQPZ6LIFpDj=w1284-h571-no)

+ Second, choose **instance** running a **Private IP** of instance which running

![](https://lh3.googleusercontent.com/S1CQUB7v2sRoAywTDjRWwdfUtu4hFVXjKYQ6BupvAKVdhI9a_XamAnlCEUUpp3MmOvKanGh51NQN86HPOCDx0RkRD_FrRAPAOah2YxW9ZPulzXzyW_4x10VkB564P6L25kzV-YpfWutI4WrX-O38EV1xH719U10kG-oT4xcCTuffhKCSix_e-tCs1OjnDk5qB_nyZigSHa5TZgbZWyi9LBnjl4PTN5ZqglTapHR5D3O1NfwKxAdk9mUaMmYXm7F83kwokS746ukQjs7uw5UzucV4CkvOxle-5PKao0vuMB1BJZllfDHIag7Ulh3GBlPduVEOVzCsos2oCOZ4bHONld8qOBLFh2dgTuyR8byjYwLkk9nT80MBS0g1vBziOWc_Xf50Iaqb-1ZNH6xLpJJwE-A9xb0NSKZMKMSj3Ty8wW7DuPV3tA5BLLbpp07KnGJTS5EAEEJUENYQKh-uG1M4_-Tv5Y9qaRegKkcCUHnhzZBUELbfFFoK1Hp7OJOTkJYJXT70fnAnwlp1SLmPJbZV8GUFFPrOETeV-mJoAof8bRVO7vHcwB1UIBS8eu_Ho_tOX1_Br2cYxRbVt6uNnTfd78Ee6JO_jjYq9x8UKhui4eqFaBBJadiHlfGx2TxwnGZGWKxXCP2WUdb9h21O2UCeFqVH1y7WCezW=w1293-h570-no)

+ Third, the public IP of instance which we want to connect, same Elastic IPs

![](https://lh3.googleusercontent.com/m6PjUEwxYOBZYhCwurDMK92DTMLsc3aSMaN6fiX_dVuOZzfn1Z15lCNGrDeu6J820d-CrgeODyghLFAPxrP97h_JL-BRQl0KYiT-FyzK6rN4etaIRJMHB8j7g_WZJnvSeTtsZBIPP7VI69YweeNVrOlEgCkNR3UH2Q5s02SjKpbc7uneAw7jYZVjk03Jz9gYwLnDufR3N5OH-l3kNIpMrH5gst-K_sl8SU9D7gU018XTfq7ZxDk2FDec9ViIrczYCBaHBpks0oHl3L26cjnlm6jxzAve2AA6E_lWsig4PlZ6jF8WhLUACeVkng0VSDonKxVXHykxMKtMUC4aVPE6DnWXjtzfdSWKq-2U0Njbe1BLpqCokHhi28R9S6k35CP2Emg7Fmx3lgzhcYYjk1ttJlaTymU7vCA0zFYiPmSGHSWCtKUwagDRV8x4oN57Try56IqL9iO5X0w0p72AzEuGDCFJcg1jYZ5K6InbqoDSjW0Z4F6wUdoXH7lVnNyUhZbU8GEHGgm9yKZTN3a1yV4InZjv76vasFaszV7gDWyARaEPMgcei-RmuyCL0IbWodCYXMF1JPGJyj5PVQgyxCx12njvncWg9pXe1LhBRdrE5TY6lNzOp8S5gORaVTe51WmwjfwPWrYMemzNQi3wmA3ytoyxE-gNn4G9=w1166-h622-no)

+ Continue, Getting **Host name (IP)** login EC2 :

![](https://lh3.googleusercontent.com/x0nICfZgSDCyx-gRjukhNJgAQO60xKw0HTjq5-R80Tbwno1bjOFsUQ1p4W-L2LoAKrkHCw9HzBaEPlQIYbXIqfimD3bhMgLEaCFQ9uzVlhyiJAisSed3kPHAfHFTpOtFm2tcOaVzFFJVPcFoAnTfu7XlViOg8QHhSafLsvI-T8AANPvyhP69M_aq4-_Jy0bY989Z6zu9gXQXfyNgZ3Z5Y2eVwjHbVhBELZdGMaktCGqvFNhuVh1xh-7hI5kojF_P60or9j6RyZwJyf9eIQ4dhT3-86A14o-gpdEyWfXHBL_4Rg9HSkG3Yzr45iiz1zOikAsIMQSoHXHyWlhyD1jB_WOsCxzCGSl3M1LepN1ZIyLTzbiN6OlznI8A2TUwS4tgGVf8podDC8jzFadWKoh5R5_fXRZtpEoRawEJlmMtq-O-C3LyKQQLL1XcKi7WIIe7X8tAwTMW6sgk0wwST-KHLEy5HhRi4EVpKdAa_yNvUeCqIlklFrOo1-3eLWiG6JkuiGNiGfWEt0lj4FSLi8lt_D_eFc2oDR-_8L885D3fi_7rLLmdhKONxNA3xZoOUhtZDjnuuzFHEk_Ho2Y-gwnKGnH0HU46IW4oz4zAjBYAK0tDiUGV-bG39YsUgyzeSo7p8g-rOfdfT1GBiqKyXVEMk-pdfoHy5WvH=w1120-h618-no)

![](https://lh3.googleusercontent.com/IgkdL6OkRWY1yfQHxt8O8P_PBIL1tGYc0jtD2b9vidcWaycCZPF3pNKMuo3TU6TO4HNtsEj-AjGkHEgTtxjueoMX18H9xqzjg9UlON4V2vXS98ab6PmWgDBIhmVs0jT9iqTjEnxG73wPVtla0mcMCMiR58HfJ6-g8ZLLFhUoZOTPB0BivTVK82m6QJ0EwsUYvU-OIxBxyftSjZWXDy61nzgGXsg0kvVr-OSUjjAmks3yfw1V3faHtfyGBR8Iaibz2LPaH2cX4Rm8y4gxB2qmJi9hdwXsX09fgIMbxJNxn8VYKr4asR_IQVLw4Zs-qfqj7f8sM2CvdKxLTYWCndr_GyzTXBGy7muvF4hkEn955DcVN8gOdcojseyVyJv7ERt24wByZwtuwKbAsIz0_hFwr5EX2e8nKxbLEkOSO_nyk5q8pQJJoyNgUSnWjh-sX4elBA_JXnOD9Nt245-CQoRFOYzGJLaShgIWql5qSl9BUzOlmw12h4ZpMwlNI1yv6p9Da1kyHFul-EoynxWYLy0oWk7_2LZfbJ0wtGzxxKIU1BLMbrM-SoVfBJzwbTEt-D4_Rh723yIZYlnrwxb5vg00PIqs6DhnRf8M2Pkmn0fbU0OuVWQPn4rmPbMBRhKnb77QlD-GH3EtRWZl6TyH0tr7zbpZUVsXoWyC=w763-h639-no)

+ Go on, connecting to **AWS** by **Putty**

Double click to **Putty**

First, choose **Sesstion** and fill in **Host name (or IP Address)** 

![](https://lh3.googleusercontent.com/QEF8VIeUzfG6ABZFdZSA71D-hF1XexwF6lhkCUwY_OjXwbOtO3JWRKCaydVIxSq6pa0Fcth69EKgCjfjTy61Ut4qtNb2cGSjKgvsWEw85sAneq5ZvGHcoJ1lkmaHf1qsyFZAb0fhiE_WMrWi29vVRUkuF3gCbFJJsaPwR1JZhF6ro1XM0vhvad0ZWFa4OxMCUViuQODA1E1qy4bfLEXGscKBu91Ldwbp301E3owm1zHkTrHzFMNa68XgxwHJPVba9udmCIGGmxzTgPftxub1qVuCWbQMogs9pxjqiKg9CbGgneZ91fO_H_e-FwxGWgR4TkUA_cA1Grxek-QRSaLku3xdrdMfIVClmeaSLgZd2yFURbGXIFq7TakUfjz2zSbTkW8FmWs92LaHj7QcUHOyPqM8ppAIsJFaOkpRlol5brQ3nxjRloX-nX2-bB81hG2iw6XBDa95N7nZnPJXatYY_VvRkrohKtEzM1wouREcVsCgnvbaJYodksynrtGqyDcrwpUD1M0eCbc-EzXc-2P3ncZ5i3uBmepqaDRNL-N1xYNPqDCJNOLUasC6ooPMa_guOtTD881zdZTqyaCS7qlD4ykcc3dOS4xUZdxo8s21h6W3VvEsuMBVhtWdP5CQrn2v426LuK3eKQmZqOg6WBmk6bnm5Blndrof=w826-h653-no)

The second, scroll down choose **SSH** -> choose **Auth** -> choose **Browse** -> Search file **.ppk** which generate from **.pem** file of **instances** and choose it

![](https://lh3.googleusercontent.com/foqhfT1BRr2WfXivqnY8J4EHMSc2NuLWUyqqXDy1egeczeGy4ZcmOtPBLuZGuJsQs5dC-4HGKSzLydU0hNlenEJhKIePYt29Kj_ZPSDLFuNKhFOhOq7YCE1TqLVkJfG79nShMXloIUh00fy03AReekZGahvoqAVtoXgaW7MSgFN9KsIH6tYC52pOaAE3wY7pqw3ke5hhgbjBJValH0-tKFtn7yh8TZaiw616ZyM8GMoA09CKj7OzXhCsUiC4TeCj5C8Aw6dJDBXu2qstA5_eOSxJq1tuoyD153xzbEXXVzALvOQGHWUg9VEcluv98w8nvThfEwXkCFaxguO06Ye_1nOB1N2d5DPnSmKOZcbgGbq9tGLZzAUXGcYXat1I-2KrcRu1gpU2J6Rnhmv08j4pSoLaEf33hpHnsjDlwA7MDEAkoJTbLAbmStmmN2Z3fZoPYKiaWeU409Jv3cXHctNipuINs4TfqScfU4uiKEGm6zrZc1tVD_smqXEeTFZfurpfJsJUng-h12Jfdx2WxXjlBIWOBgu1d0j6rM2c44WJ4yocOpqIVnxAe_KfBSM46HHYf9B9YhH6ZhPYUX_wwqGd3NFEXtipfNGWpS77tDMwwJSvdmjTO6j_kMc9mP1Phcz9PX4lelbPwrLNdYYJHf-O0FQFoLF4Qt22=w621-h653-no)

Finish, click **open** button to access VM **instances**

![](https://lh3.googleusercontent.com/bZp9XUF9cGoVPVwEnX4P4GwFMD4sxRUNBNV5EsCu2gvvp6oK1BYbcJpoDW7uxAK_P42z1zTfASmKY2VnEyU2vgcE9iJHtm2_LzH_HGgrUO-XcSxYFSvldovcnIJygiQQFhgnYysEmkeYJwJwY-bb0BAPOThrNw6uswu4ynjuykMFaDhk25ydzUzjes5uZYpg3UTAcm0Y2wxpdaUbK52XUdf79OG6gweZe2QYVwSj9h32SDWhtw8AVuvQC-KW0fC41rD4ClzkPKBfjNybbjxhwYadH43AVDdl7h9kXtbVNmmvMVHclL7T071Ev6YEazdnAoT0C4GkAstHuMwcncQ9EI1L_4zz9qXSzYjLrNMW_-y3le83ieAvbcjWvWQ6LDk3eNbpjXIJ3T1vrFSHGcpg8TKCaAYP7WGbpudhH4uOlK_pIqVK0oHfN4Obm4P7iQjYemDdmf55ByBezwCBx1YsE2sXvVQpihkgqAiszSI5myCh2enGQUeGdCBC8nou2optdn1PQROn8cERApwqKPE0qb5V47sxpazo0jc39Bv9n1t_sBX-mRcM6Q-MV24UfqjJzCA4xaMvPan_5bUanP5UvsyIZt4Sld2GsRK1ox5g-MzwzXVPfIGnOgrBOsbW6--tXCzhU5rQILUMVn6AqiYwbZG5ayS1pVwi=w653-h428-no)

### Some error connect Mysql AWS:

+ **Can't connect to local Mysql server through socket**

```
    mysql -u root -p
    Can't connect to local Mysql server through socket '/var/run/mysql/mysqld.sock'
    
    ls -lart /var/run/my*
    ls cannot access /var/run/my*: No such file or directory 
    
    mkdir /var/run/mysqld
    touch /var/run/mysqld/mysqld.sock
    ls -lart /var/run/my*
    chown -R mysql /var/run/mysqld
    
    mysqld service restart 
    mysql -u root -p
    password:
    
    mysql > quit
```

+ **Access denied for user 'root'@'localhost' (using password: Yes) after password reset LINUX**

```
    service mysqld stop
    mysqld_safe --skip-grant-tables
    
    mysql -u root -p
    mysql> use mysql;
    mysql> update user set authentication_string=password('password') where user='root';
    mysql> flush privileges;
    mysql> quit
    service mysqld restart

```
**'password': Your password which you want to update**

**Install package in Red Hat Linux**
+ yum install package_name

**Remove package in Red Hat Linux**
+ yum remove package_name
+ yum autoremove 
+ yum autoclean

## Install Wordpress in Red Hat Linux

+ sudo yum update
+ sudo su
+ yum install httpd
+ service httpd start
+ yum install php php-mysql 
+ yum install mysql-server 
+ service mysqld start
+ mysqladmin -u root create myblog **(haven't password yet)**
+ mysql_secure_installation
+ > Enter current password for root: space
+ > Set root password: [y/n] y
+ > New password: admin
+ > y
+ > y
+ > y
+ > y
+ cd /var/www/html
+ wget http://wordpress.org/latest.tar.gz
+ tar -xzvf latest.tar.gz
+ mv wordpress myblog **(copy and change name folder)**
+ cd myblog
+ mv wp-config-sample.php wp-config.php
+ vi wp-config.php
+ -------------- Press i for inserting ---------------
+ db: myblog
+ user: root
+ pass: admin

![](https://lh3.googleusercontent.com/yUAtnbqTIjqLdrXNkFEtZg8MhEVmc6estyvveLonUaJkRsiujXYDELcekLe_gACHQkfdiVq6oI9YSx58SGcGfUV78AEEckoViStvrxIthaJ8Q8xm1wXr9kR_uGcQH5MGF0A8eiWiKVI0USoYwpmJ_Yy4ZUC8v5hegsyj2VpGM30qABd_yFrdEY-OgvV7VILK2wt8IK0ZWR90TeJuEsWqytfEqg0-FWv0g0ixw7z7QFpk3gCkn2ufTgrnNPdQwP3qs8mfTv5TxzwmCz_JuuTgeDran9HKznmopMxknRWh_nuyocV4Swu6torVhWLpqG5iytXoqEDwtV0M07pgsHQ8qsFW3CcCCTVob7CKvFTp2fz6yMTBLUyJrKysEsvJpXcfqsQAqa_pZYaKH2EUQmpfxMhQJOz5JNf_6ybAV-f-H_ArBPS7__abdITrhXLmxHrhOhE6Z3zrK-JFgUDP4kEz72_pQGPfXrvPhhjmMRc7RTxcdkWaLc6zc9I2ZxYSWUw6DYEnNppgZ7QmRq4lSkHHcu-SqDJwOyMxDji4MdwJT5VbXyRRD09tO9lpS0bXezc_su5d-sFPce3e7qBAax4tHA5paRirl783yv8y9YV5hWmfYRNVgOonAr5fOYk9ALjfQVOULuCy7mRJQ9B37NRfDyCIA3xHFiLP=w627-h416-no)

+ -------------- Press ESC + wq ----------------------
+ httpd -k restart 
+ http://PUBLIC_IP/myblog


![](https://lh3.googleusercontent.com/HlblvHcOc7uKHIWCH4oSIDX-yqIMrTom7q_CP7_mv4ePsNMC_tZJKExiyRZJC6x8rxcTMo-nPqanW1Negs92Tl41E_lPtExMCEwqkNB_YCXbDWO6Eq1Rg0NkIJ7xaADASyjn1oKvTJibjeaLSpmty7Ny6-OaEmGRkcrplix4r1f7wpKzke-xq7BfA-_zdcxtXaExbK1BHJ1q7RLz3b9hhovfKP3BfJMDEsb7zD2344kdEFF8S6Tz625lw3jsoX_OeFF39i__XJ_fKNDTdA236Murx3mxG-6DkQCtY7knR_oYNEssS-ZCjcwPLV-HUbT3RXQwbCNm7f31x3a-5-C2m723HKjzyYxen-qWcuxgumiw0ElzOBHz5gejhLJEqg6GXxuhptNKEGAiiRDmwMgaiZACzK6fSXRMfi8ZNmW5-5Dql7m17I5J-FWa3_QSdsN3RlPRMhOWQBxc5dqQRgI6rK3qyhJIhgeeElBJhsRZWnNUwEPfOwpb_tjygHauq7U_94pGTZHIWbzxEdcQkQdxZ_iBkJXcu4BVrvjOG0NQesZqiigMQhXoEMIJCBGRKnhprPWzeNZfkh48k9VLYP5APbncRcKsWxr2mPxOYHCMiYlrZCHA72od2GvGtuzrfBdPry0FjhaTHmNyjDA04OeNfrbbHkNZLvlb=w1086-h702-no)

**username**: hauntps

**password**: 09030244360903024436 