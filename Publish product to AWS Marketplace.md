### 1. Partner Seller Registration:
This is the initial step for getting started. AWS provides  procedures outlined for US based sellers, EU and non-US or EU based sellers. There is no need to provision tax/bank account information for free, BYOL AMIs or SaaS offerings.

Link: [https://aws.amazon.com/marketplace/management/register/](https://aws.amazon.com/marketplace/management/register/)

### 2. AWS Marketplace management portal:
This portal page provides the required Marketplace forms, file uploads and managed products. All inputs and file uploads must be done through this portal to the AWS Marketplace team for AWS Marketplace launches. This helps ensures security for the customers as uploaded files are scanned for vulnerabilities.

Within the portal, custom policies can be created for managing the AWS Marketplace portal and they can be attached to user groups. A user must be given full access to manage the products page “aws-marketplace-management.*”  However, for accessing the different pages like marketing, support, reports and upload files, custom policies can be specified for the required combination of permissions.

### 3. AMI Readiness: 
The software distributed should be well tested and production ready. The software to be launched needs to be packaged as an AMI and well tested in all the regions you intend to release.  You should also share the AMI with other accounts and ensure the shared AMI works in other accounts within the same region to avoid issues with permissions and account dependencies. Updated AMI’s are submitted through [the self-service scanner](https://aws.amazon.com/marketplace/management/manage-products/?#/manage-amis.unshared).  

### 4. Product metadata and data load form:
This is an excel spreadsheet that needs to be filled-in. It contains both required and optional fields. This spreadsheet is the main documentation that drives almost all the details seen in a marketplace listing. This template is used for all types of AWS Marketplace product launches ( paid subscription, free, SaaS or BYOL) [Commercial product form template](https://s3.amazonaws.com/awsmp-loadforms/ProductDataLoad-Current.xlsx). This form, which contains the new AMI or updated details, needs to be loaded through [the file upload tab of the seller management portal](https://aws.amazon.com/marketplace/management/product-load/).

### 5. CloudFormation Templates (CFT): 
Whenever an AMI is launched from the Marketplace, it internally invokes the CFT to provision the product. The AWS Marketplace user interface provides two  tab options “One-click launch” and “Custom launch”.

### 6. Topology diagram and metadata:
You will need to create an architecture diagram that is focused on the different AWS services and resources that will be created for the product using the CFTs. The diagram should leverage AWS icons for each AWS service being deployed. The diagram will need to be 1100×700 pixels and must use [a set of approved icons](https://aws.amazon.com/architecture/icons/).

### 7. Pricing estimation:
Software price: This information needs to be provided as part of the documentation and should include all the instances created by the templates. Prices can be by hourly, annual subscription or however it is structured for the products licensed.

In **summary**, the following artifacts are required for the launch of a product in the AWS Marketplace:

+ AMI upload

+ Product data load form

+ CFT (Cloud Formation Template)

+ Topology Diagram

+ Topology Metadata

+ Save & Share link of the AWS simple monthly calculator for the AWS resources

+ Software pricing table

References: [https://docs.aws.amazon.com/servicecatalog/latest/adminguide/getstarted.html](https://docs.aws.amazon.com/servicecatalog/latest/adminguide/getstarted.html) 





