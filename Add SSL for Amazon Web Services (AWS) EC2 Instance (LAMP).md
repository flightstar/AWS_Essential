Kết nối Putty đến EC2:

```
    wget https://dl.eff.org/certbot-auto
    chmod a+x certbot-auto
    sudo ./certbot-auto --debug -v --server https://acme-v01.api.letsencrypt.org/directory certonly -d your_domain.com (domain ssl đã mua )
    2
    1
    /var/www/html/
    
    
    sudo nano /etc/httpd/conf.d/ssl.conf
```

+ Tìm dòng `SSLCertificateFile /etc/pki/tls/certs/localhost.crt ` và comment dòng đó lại. Và thay dòng đó bằng dòng `SSLCertificateFile /etc/letsencrypt/live/your_domain.com/cert.pem` 

+ Tìm dòng `SSLCertificateFile /etc/pki/tls/private/localhost.key ` và comment dòng đó lại. Và thay dòng đó bằng dòng `SSLCertificateFile /etc/letsencrypt/live/your_domain.com/privkey.pem`

+ Tìm dòng `SSLCertificateChainFile /etc/pki/tls/certs/server-chain.crt ` và comment dòng đó lại. Và thay dòng đó bằng dòng `SSLCertificateChainFile /etc/letsencrypt/live/your_domain.com/fullchain.pem` 

+ Save lại

+ Restart lại: `sudo service httpd restart`


