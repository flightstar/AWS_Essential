Thực hiện trên Ubuntu 14.04

+ **lxd**: (Linux Container Supervisor): Là một trình quản lý các container Linux thuộc thế hệ tiếp theo. Nó cung cấp một trải nghiệm user tương tự như 1 máy ảo nhưng là bạn đang sử dụng Linux Container thay thế. Nghĩa là cài Open stack lên lxd giống như cài lên máy ảo để có thể ảo hóa.
+ **zfs**: ZFS hiện tại vẫn đang trong giai đoạn phát triển bởi Oracle với nhiều tính năng tương tự như Btrfs và ReiserFS. Hỗ trợ tính năng pool trên ổ cứng, tạo và lưu trữ snapshot, nén dữ liệu ở mức độ cao, chống phân mảnh dữ liệu nhanh chóng... được thiết kế riêng biệt dành cho các doanh nghiệp có quy mô lớn.
+ **juju**: Juju là một công cụ mô hình hóa nguồn mở, hiện đại nhất cho phần mềm điều hành trong đám mây. Juju cho phép bạn triển khai, cấu hình, quản lý, duy trì và mở rộng các ứng dụng đám mây một cách nhanh chóng và hiệu quả trên các đám mây công cộng, cũng như trên các máy chủ vật lý, OpenStack và containers. Bạn có thể sử dụng Juju từ dòng lệnh hoặc thông qua giao diện đẹp của nó.
+ **khái niệm cơ bản về hệ thống file trong Linux**: [Tìm hiểu khái niệm cơ bản về hệ thống file trong Linux](https://quantrimang.com/tim-hieu-khai-niem-co-ban-ve-he-thong-file-trong-linux-84900)

`do-release-upgrade`: Update Ubuntu 14.04 to 16.04

Install Openstack:
+ `$ sudo apt-add-repository ppa:ubuntu-lxc/stable`
+ `$ sudo apt update`
+ `$ sudo apt dist-upgrade`
+ `$ sudo apt install lxd zfsutils-linux`
+ `sudo lxd init`

```
Name of the storage backend to use (dir or zfs): zfs
Create a new ZFS pool (yes/no)? yes
Name of the new ZFS pool: zfslxd
Would you like to use an existing block device (yes/no)? no
Size in GB of the new loop device (1GB minimum): 50
Would you like LXD to be available over the network (yes/no)? no 
Do you want to configure the LXD bridge (yes/no)? yes

NẾU YÊU CẦU NHẬP IPV4 THÌ NHẬP PUBLIC IP CÒN LẠI MẶC ĐỊNH
LXD has been successfully configured.
```
### Disable IPv6 on all the network interfaces:

+ `$ sudo sysctl -w net.ipv6.conf.lxdbr0.disable_ipv6=1`
+ `$ sudo sysctl -w net.ipv6.conf.enp2s0.disable_ipv6=1`


For name we will use zfs as storage backend (that’s the purpose of this tutorial and to benefit from all [ZFS advantages](https://youtu.be/lM2wwYDLB2M?t=13m40s) in lxd containers). For a new system, there will be no prior ZFS pool configured so it must be created and named (first yes and zfslxd). I won’t use an partition or hard disk so i answered no so that setup will create a 50 GB file as the ZFS pool. Don’t need to use these containers on external network so i selected no and then to start LXD bridge setup yes.  Then only default values until user is returned to terminal (i don’t need IPv6 so that one i skipped).

Next lines will show some overview of the ZFS storage pool and initial lxd configuration commands 

```
$ sudo zpool list
$ sudo zpool status
lxc config get storage.zfslxd
newgrp lxd
```

The next step (and last one) is installing conjure-up and OpenStack – automatically and with minimal intervention.

[Build your own production private cloud](https://www.ubuntu.com/download/cloud/build-openstack)

```
sudo snap install conjure-up --classic (Install conjure-up using snappy:)
conjure-up (deploy OpenStack NovaLX using conjure-up)
Chọn Openstack With Nova-LXD -> Localhost
```

Install took me ~25 minutes. Maybe more or less on other tries. During install you can open a new terminal and see containers that run and their IP addresses with following command 

```
$ lxc list
```



