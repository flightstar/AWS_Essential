## 1. Định nghĩa
Là một dịch vụ cho phép kết nối một phần mềm on-premise với hệ thống lưu trữ trên Cloud một cách liên tục và bảo mật. 

Phân biệt phần mềm on-premise với on-demand (cloud)

+ on-premise: Tiêu thụ tại chỗ, được cài trên phần cứng máy tính, server, kết nối tới server nào đó được quản lý bởi nhân viên IT
+ off-premise: Mang về nhà tiêu thụ
+ on-demand: Các dịch vụ phần mềm Sass

![](https://lh3.googleusercontent.com/T8Fb1tYd5MzWRyrD-KanU_fJratkmtQddl84PzMP-rjImGyC7g_U1tGSlmfu1rRmfmEzoMycnA_RZZVQSa6vSf8cbdlJObcB_yhGlOMadbKrpC3zaeFsLdEh8Litq9JYnZ9b27nySpZNnNjBlcIb-7kQo3xCng_CCIWyplklSeE4q2-1TZrONOoJKsk3qSOfnaZntqYY2ZQLbH1GtYdaF5TUbzJ5fw2fXHsBFWQ0bflzhyZyt_pIpiZwaMS_uI2FhIzDOpPyLjdJ0rWIWK2tkAjvKZVYrMGRaCBQvuTa0yKTYWA8EHyGfSidLU26qMZLBAiLfmw-sMyIwLT0Vnpr6vFEha0Zw2wR9kgxu8SbeZzHX2zsfTgVG--og07IG9TQlOQlcXJ7whokx_TmjYR-CTMPjXqQsGJ1zVeXvHpiLXMQPceZ6hc8J8GafzYAWi550mUp-4eBBrj3IRcXn1xOou75KANLOjLSWLrCFtAtgkys0YqIDfHIxV_BPsTIeHSXLvJkqKDtGOJjBdA_JI1f27aIkKD2WGI6T1TA3_3kWsTDQNy_L6B8lOQ6ldmnenWxyMgAO0cPG-iHP_LR8Dsg2XiE2s1p2w8cioq-6inD4CHwXWYWa9VujEoIZcntc6kyZD31WnlSZs7JN7Xks_MbC35VZooBLEwI=w930-h476-no)

**AWS cho miễn phí 100GB từ lúc đăng ký đến cuối đời**

**Click vào để xem hướng dẫn**



[![](https://lh3.googleusercontent.com/JYeQsjxefKrrBu3vWfzCncp7r5M3UIapwbrvzVaA9Ol1aX6zHd_Pko3qiRflCMTCZmus2dDW70hy-WF7cLqHv2qWvGzb1PmIcXjgbY5pyl0iy9fMk2Nt8wAu5koEdDfizrH4MJKSeMaDA-Yl-BMhyK6Zf3_BqxUwEu743KmGr9sG2MghG2QLml9BMMITLkrwHa87M5XME6Ki6uFCSRWB1EBQwBdS-l6lhL5sK4UMQoOJ3OhLUN9dem258cjfn8xAW0AU8jhn-1MEfKQFQWioI4SxkcuhzMO5yQANDZyX2SNJIeU19P2y7XFIlVHtl6gue4qRSTu9huwEMEohchzvZBFNXjB79cgjYUX6o-h1Y1MrrS3FFA2HegP3dQAKlcWAtUZRCOs_MHN1uEk587ItUNJ6RLOtKrtbqk5YSatPD4vrJdB5AGaA1WolpVgmDIWyDE8FMjX0DeMp3gXKACbyq48fWmoh8SWxpBKAIm_oshN1NhdEw7QHtQSUWVEJ5uVdNk_-mtV9psv1NLeoxfsg5lkcMrIfQFW6j9OtksP0CC-YjqQbU8HVbrLf-dhjymZWPWJ7Q7AuhO7hcV9FVqV-DvTguxYtyMUzRZna2k_5aJNXTnpXtCU7tg66qsW3OqogjSWjJVYT8_7M_yl-RiepPuMj8Fw-r00t=w648-h299-no)](https://youtu.be/nTxaP8OqLkQ)