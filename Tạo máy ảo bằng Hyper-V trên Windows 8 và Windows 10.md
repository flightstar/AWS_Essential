## 1. Tạo máy ảo bằng Hyper-V trên Windows 8
### 1.1. Kích hoạt Hyper-V
Nhiệm vụ đầu tiên bạn cần làm là kiểm tra xem phần cứng có tương thích với chương trình hay không. CPU máy tính cần hỗ trợ SLAT.

Công cụ tốt nhất để kiểm tra xem CPU AMD hay Intel có hỗ trợ SLAT không là [CoreInfo](https://quantrimang.com/url?q=aHR0cDovL3RlY2huZXQubWljcm9zb2Z0LmNvbS9lbi11cy9zeXNpbnRlcm5hbHMvY2M4MzU3MjI%3D).

![](https://i.quantrimang.com/photos/image/072012/21/mayao1.jpg)

Bước tiếp theo là kích hoạt Hyper-V trên Windows 8. Theo mặc định tính năng này bị vô hiệu hóa.

![](https://i.quantrimang.com/photos/image/072012/21/mayao2.jpg)

Sau khi đã kích hoạt **Hyper-V**, khởi chạy **Hyper-V Virtual Machine** bằng cách, từ màn hình Metro Start, tra Hyper-V và kích vào biểu tượng Hyper-V Manager.

![](https://i.quantrimang.com/photos/image/072012/21/mayao3.jpg)

Để truy cập dễ dàng hơn, cho hiển thị các công cụ quản trị trên màn hình Metro Start để truy cập tới các biểu tượng **Hyper-V**. Chọn **Hyper-V Manager**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao4.jpg)

### 1.2. Tạo máy ảo mới
Chương trình **Hyper-V Manager** được mở trên màn hình desktop.

![](https://i.quantrimang.com/photos/image/072012/21/mayao5.jpg)

Trước tiên, hãy tạo một switch ảo đóng vai trò cổng Ethernet ảo sử dụng card mạng máy tính chủ. Trong khung **Actions** ở bên trái, kích vào **Virtual Switch Manager**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao6.jpg)

Bây giờ, đặt loại switch là **External** để nó sử dụng được cạc mạng NIC. Sau đó, kích vào **Create Virtual Switch**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao7.jpg)

Tiếp theo, đặt tên cho máy ảo theo ý người dùng sau đó chọn kiểu kết nối mặc định tới card mạng đã cài trên máy tính.

![](https://i.quantrimang.com/photos/image/072012/21/mayao8.jpg)

Chọn tên máy tính chủ (máy tính đang chạy Hyper-V). Sau đó dưới khung Actions, nhấn vào **New >> Virtual Machine**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao9.jpg)

Cửa sổ cài đặt máy ảo được khởi chạy, trên màn hình xuất hiện giao diện Before you Begin đầu tiên. Nếu không muốn thấy giao diện này trong những lần tạo máy ảo sau, tích vào **Do not show this page again** sau đó nhấn **Next**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao10.jpg)

Tiếp theo, trong phần Specify Name and Location, đặt tên và chọn vị trí lưu máy ảo.

![](https://i.quantrimang.com/photos/image/072012/21/mayao11.jpg)

Trong phần Assign Memory, chọn dung lượng RAM cho máy ảo. Hyper-V chỉ cho phép người dùng sử dụng một lượng RAM chỉ định. Nếu đặt cao hơn giá trị này, thông báo lỗi sẽ xuất hiện.

![](https://i.quantrimang.com/photos/image/072012/21/mayao12.jpg)

Tại phần Configure Networking, chọn tên switch ảo đã tạo trước đó từ Menu Dropdown.

![](https://i.quantrimang.com/photos/image/072012/21/mayao13.jpg)

Tiếp đến, ta phải tạo ổ đĩa cứng ảo và chọn dung lượng cho ổ cứng ảo này trong phần Connect Virtual Hard Disk. Ở đây ta đang tạo một ổ ảo dung lượng 40GB. Bạn cũng có thể sử dụng một ổ đĩa ảo đã tạo trước đó rồi hoặc bỏ qua bước này.

![](https://i.quantrimang.com/photos/image/072012/21/mayao14.jpg)

Bây giờ, chọn phương thức cài đặt. sử dụng ổ đĩa từ máy chủ hay một file ảnh ISO để cài. Ở đây, bài viết sử dụng một file ISO lưu trong ổ cục bộ để cho kết quả nhanh nhất.

![](https://i.quantrimang.com/photos/image/072012/21/mayao15.jpg)

Một màn hình tóm lược hiện ra để người dùng xem lại các thông số, sau đó nhấn **Finish**.

Giờ là lúc cài đặt OS cho máy ảo. Lúc này, máy ảo được tạo đang ở trạng thái tắt. Kích chuột phải vào **State**, chọn **Connect**.

![](https://i.quantrimang.com/photos/image/072012/21/mayao16.jpg)

Máy ảo sẽ được bật. Kích vào nút **Start** màu xanh trên đầu cửa sổ để khởi chạy máy ảo.

![](https://i.quantrimang.com/photos/image/072012/21/mayao17.jpg)

Máy ảo khởi động mà chưa có hệ điều hành Windows nào được cài. Đặt bất cứ OS nào người dùng muốn vào máy. Ở đây ta sử dụng Windows 7.

![](https://i.quantrimang.com/photos/image/072012/21/mayao18.jpg)

Bây giờ, ta sẽ tiến hành cài đặt Windows 7 cho máy ảo. Quá trình diễn ra hoàn toàn giống với quá trình cài đặt trên máy thực.

![](https://i.quantrimang.com/photos/image/072012/21/mayao19.jpg)









## 2. Tạo máy ảo bằng Hyper-V trên Windows 10
### 2.1. Kích hoạt Hyper-V trên Windows 10
Mặc dù Hyper-V được tích hợp trên Windows 10, tuy nhiên bạn phải kích hoạt Hyper-V thì mới có thể sử dụng được. Để làm được điều này, đầu tiên bạn nhập **"Turn Windows features on or off"** vào khung Search trên Start Menu rồi nhấn Enter để mở cửa sổ Turn Windows features on or off. Hoặc cách khác là mở cửa sổ lệnh Run, sau đó nhập **"optionalfeatures.exe"** vào đó rồi nhấn Enter.

![](https://i.quantrimang.com/photos/image/2016/09/01/turn-windows-feautures.png)

Lúc này trên màn hình xuất hiện cửa sổ Windows Features. Tại đâu bạn đánh tích chọn **Hyper-V** rồi click chọn **OK**.

![](https://i.quantrimang.com/photos/image/2016/09/01/hyper-v.png)

Windows sẽ áp dụng thay đổi và sẽ yêu cầu bạn khởi động lại hệ thống để hoàn tất quá trình. Do đó bạn nên lưu lại tất cả mọi hoạt động rồi click chọn nút **Restart Now**.

![](https://i.quantrimang.com/photos/image/2016/09/01/restart-now.png)



### 2.2. Tạo máy ảo (Virtual Machine) trên Hyper-V
Sau khi hệ thống khởi động xong, **Hyper-V** sẽ tự động được kích hoạt. Để tạo một máy ảo, trên khung **Search Start Menu**, bạn nhập **Hyper-V** vào đó rồi nhấn Enter. Trường hợp nếu không tìm thấy Start Menu, bạ có thể tìm kiếm **Hyper-V trên Control Panel** => thư mục **Administrative Tools**.

Bước tiếp theo bạn cần làm là tạo một Virtual Network Switch để máy ảo mà bạn tạo ra có thể truy cập được mạng Internet. Để làm được điều này, bạn click chọn tùy chọn **Virtual Network Switch** nằm dưới mục **Actions**.

![](https://i.quantrimang.com/photos/image/2016/09/01/Virtual-Switch-Manager.png)

Tại đây bạn chọn tùy chọn **External** ở khung bên phải rồi **click** chọn nút **Create Virtual Switch**.

![](https://i.quantrimang.com/photos/image/2016/09/01/external.png)

Đặt tên cho **Virtual Switch** của bạn rồi chọn kiểu kết nối là **External network** và chọn card mạng từ Menu Dropdown. Đánh tích chọn **“Allow management operating system to share this network adapter”** rồi click chọn OK để lưu lại thay đổi.

![](https://i.quantrimang.com/photos/image/2016/09/01/dat-ten-network.png)

Trên màn hình bạn sẽ xuất hiện một cửa sổ cảnh báo. Chỉ cần click chọn **Yes** để tiếp tục.

![](https://i.quantrimang.com/photos/image/2016/09/01/canh-bao.png)

Sau khi hoàn tất các bước là bạn đã tạo xong một máy ảo. Để mở máy ảo, kích chuột phải vào tên máy tính của bạn ở dưới mục **Hyper-V manager** và chọn **New => Virtual Machine**.

![](https://i.quantrimang.com/photos/image/2016/09/01/Virtual-Machine.png)

Cửa sổ New Virtual Machine Wizard sẽ xuất hiện trên màn hình, bạn click chọn **Next** để tiếp tục.

![](https://i.quantrimang.com/photos/image/2016/09/01/chon-next.png)

Tại đây đặt một tên cho máy ảo của bạn rồi click chọn Next. Nếu muốn lưu máy ảo ở một vị trí khác, bạn đánh tích chọn **"Store the virtual machine in a different location"**.

![](https://i.quantrimang.com/photos/image/2016/09/01/store-virtual.png)

Nếu hệ điều hành khách (guest operating system) bạn muốn cài đặt là **64-bit, chọn Generation 2**. Nếu muốn cài đặt hệ điều hành khách phiên bản khác, chọn **Generation 1** rồi click chọn **Nex**t để tiếp tục.

![](https://i.quantrimang.com/photos/image/2016/09/01/generation-2.png)

Trên cửa sổ tiếp theo, nhập dung lượng bộ nhớ mà bạn muốn cung cấp cho hệ điều hành khách.


![](https://i.quantrimang.com/photos/image/2016/09/01/bo-nho.png)

Tiếp theo chọn Virtual Network Switch mà bạn đã tạo trước đó từ Menu Dropdown. Nếu không chọn bất kỳ kết nối mạng nào, bạn vẫn có thể cấu hình sau khi tạo xong máy ảo.

![](https://i.quantrimang.com/photos/image/2016/09/01/connect.png)

Lựa chọn các tùy chọn mặc định rồi click chọn **Next**. Nếu muốn bạn có thể thay đổi kích thước ổ đĩa.

![](https://i.quantrimang.com/photos/image/2016/09/01/o-dia.png)

Đánh tích chọn nút **“Install an operating system from a bootable image file"** và chọn ISO bằng cách sử dụng nút Browse. Trong ví dụ dưới đây là cài đặt hệ điều hành Windows 10.

![](https://i.quantrimang.com/photos/image/2016/09/01/install-operation.png)

Cuối cùng click chọn **Finish** để hoàn tất quá trình.

![](https://i.quantrimang.com/photos/image/2016/09/01/finish.png)

Trên màn hình chính, kích chuột phải vào máy ảo mà bạn đã tạo và chọn **Connect**.

![](https://i.quantrimang.com/photos/image/2016/09/01/connect-1.png)

Lúc này bạn sẽ nhìn thấy giao diện máy ảo trên màn hình. Tại đây bạn click chọn biểu tượng nút Nguồn để mở máy ảo.

![](https://i.quantrimang.com/photos/image/2016/09/01/nut-nguon.png)

Thực hiện theo các bước hướng dẫn trên màn hình để cài đặt hệ điều hành khách (guest operating system).






