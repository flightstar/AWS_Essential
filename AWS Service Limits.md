I need to launch a Amazon EC2 instance (Red Hat Linux 64bit), in particular a GPU accelerated one. I already tried with free tiers using t2.micro instances and configure everything is fine. But when **lauch** instances, i get error:

`
You have requested more instances 4 than your current instance limit of 1 allows for the specified instance type. 
`
Solution: Report to AWS
### Viewing Your Current Limits
Use the **EC2 Service Limits** page in the Amazon EC2 console to view the current limits for resources provided by Amazon EC2 and Amazon VPC, on a per-region basis.
**To view your current limits**

1. Open the Amazon EC2 console at https://console.aws.amazon.com/ec2/.

2. From the navigation bar, select a region.

![](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/images/EC2_select_region.png)

3. From the navigation pane, choose **Limits**.

4. Locate the resource in the list. The **Current Limit** column displays the current maximum for that resource for your account.



### Requesting a Limit Increase
Use the **Limits** page in the Amazon EC2 console to request an increase in the limits for resources provided by Amazon EC2 or Amazon VPC, on a per-region basis.

**To request a limit increase**

Open the Amazon EC2 console at [https://console.aws.amazon.com/ec2/](https://console.aws.amazon.com/ec2/).

From the navigation bar, select a **region**. Please choose correct **region** which you want to increase

![](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/images/EC2_select_region.png)


From the navigation pane, choose **Limits**.

Locate the resource in the list. Choose **Request limit increase**.

Complete the required fields on the limit increase form. We'll respond to you using the contact method that you specified.

![](https://lh3.googleusercontent.com/uQhjW5wpmMDN5f9Wyife1cMCPYgut6qvCszm2gWeDmXA6EfZjUKNO4jplITFdIosy3wpiSKfuyAmga34ikDzC__6OlGYaLaAOHAUCPzpJDpNdSR50VJgd3j_MytsM3xftnoV1mq9x-tn8N2jIXKsfyYdPjMheyDV4ABkkV-h079hp1IM5qYlkSdIKBRTr-9bTdMWTTK16xBKcyLI5YtFIpM21mqsK0f_9DtatliOOPVyKS3HR2QAhgjCiWU_J7jL43jy26AJ0Sn3mYwc8AYXUORFphe5zg6kiZCng349a4bZz6Nk18qy-JtWIuZEvehhU43AtOGkUiMflznyH8Cas5AGN0U9rB7NrlN2xT9FOVHS0jT7nyAiSl7rw5IyE1b7S9H7oX_53Tv6Xd8jSDJxmXb7CcPVnW2PTn4T9Y4ahsvvTYYhsP6VQrfVm-JE62iFtAv6a0YqmfEPXOMU5OHZo-EdeRoJR5SbRdnenEEoabib4H1VXl-bhwauF0SqTRiPUhNqE5EQGpCXVzlTtLfw8Z8y-xI8kZLMXteIAImdrtriYQHAP3dIeujGWn9ISSJsgb-zpFNvqaw2f7WMfCMvDtdUEAg74SMDBrxJ3MUNz_2p0a1S8iERlamKQm18wbCM9gTpLSVRMVlhuyu8uAVi1og9UQhwvXqH=w1266-h652-no)

Please fill in form correct about **Regarding: Service Limit Increase, limit type: EC2 Instance, region: US East (Ohio), primary instance type: t2.micro, limit name: Instance Limit, new limit value: 20**. And **desciption** like as a message will send to AWS

![](https://lh3.googleusercontent.com/kOj1Xy2t-FAlaQUP3rTSjsTVizcY5DwKOfFtJbvaRJhr-dYu2fveYU4tcjP2ygCjluTjJ-Nf27pWfhajSxGs9diRfCEMz9_IGeGgy6SNdSpaRzcXdU79B9j-4FxFJwh6f_UXUq23Qg0Qvd3AhVzPrvwZ-cd5Zfhu7bKE3I_fPjCnLhquKMk6bMxfZTq-7klOQIYienB76qyYUSr-CGSKfSpK1eZOjz0Xsg9avhK4_SSW6MzUIW5wRthLRo370oxuXHbQaWsyIIjviKKWm5M5iSnfjHMT8gYPS4P1SsgB4Cq_sFRGdCS_ng0h4hy1tyRobcWAVeZUdGB4BlJnbCG9FZ3o0h-_S9jQj1WOY5uyHBh64pcxCcRXD81tYF-kInOhnElRCNq1d518cwTUlmuQCwF1bRr3TR8oOMkMx0GDqQYOpXWsYH4ow4Rfek95k31M77wcAdvbQhwBTdLEOSP1fJHOF4yo2JxAThGx1II28Og49L3cf5FQNsz8RQj8tKwhvOOms1ippTSs7HvGnK_KUOiAl8ZCcdBZh4eFtuArclpeKa6KViq4vjiI-u1-KBAwCl7I-pd_1LsKPUhckwQvWCkeKBZ4uZ7Zy-PludHpeAkjwVAaSSUJaERh1UWuxHdBFvtPeZvBEXb4kriwH95KArH15TpcbPK8=w1231-h667-no)

Click **submit**

You wait for AWS about 48h to resolve problem about "Increase instances EC2" for you

![](https://lh3.googleusercontent.com/Ol1OQybd7CENHBHollf10znyQb0F4sAILFc4oCHumFfLWqaDW_cxCaEx1BbpWs2DMu206BRG7X8fbvsEcSgQuV09W1fvcAay1JLzYDmIytM7LFr3x-XVGZ2tw4fmxR6nwAhvrGoTSicKHRuaYGG-rHUphNF2PMBvls8mEZJrAGNoWH3VR6kpur9l96aXcqalaJdoyyI1v0bI9ZboVJDGUa_b_uq7OUrP5JQNNbE-chWvIKD3veFdYjkjHeW62ML_3pIjjFGJXKCvafdlsnFw7siWSuTzNN8aLaN0Ek6b29yb7HTMfO5mCGNQscejEebq7koCVAtI8iBCh0oZO6tHrgtCNBlwb2kqgWA42ILwx64EPSxi0ezLL9e32mqPAs3MO35Fx5Q0w9ZJ5wXFEedzSpMbuPo0YRdGNA_OExQ9NtSdhDHe0r5prSZo1iNlf8PuztkMBPKDUK7A6jtqLxQF4rbO_6lqqqsvO1CvIcLkAMVWqp_GskWPzIHncTOMIW2UWDIacP9SwLKLeyHcjHHkxXGJUGhDDtAWziJCixiItZJRDK1fM5maY8F-_QEfX4jqnaizI-4eAiCxm5DykDCsmPhml6mUYSqqyBuH7AUMk5dzVZj3v2KSQn7FHqUv43YbnBqLgPTJKUth0x4W705QDopa-AOe02Z3=w1046-h666-no)