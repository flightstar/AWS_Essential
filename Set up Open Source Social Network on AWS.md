1. Install LAMP:

+ How To Set Up mod_rewrite for Apache on Ubuntu:

```
sudo apt-get update 
sudo apt-get upgrade
sudo apt-get install apache2
sudo a2enmod rewrite
sudo service apache2 restart
sudo apt-get install php7.0-cli php7.0-common php7.0-json php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-curl php7.0-zip php7.0-gd
sudo service apache2 restart
```

Set up .htaccess (HyperText Access):

We will need to set up and secure a few more settings before we can begin.

First, allow changes in the `.htaccess file`. Open the default Apache configuration file using `nano` or your favorite text editor.

```
sudo nano /etc/apache2/sites-enabled/000-default.conf
```
Inside that file, you will find the `<VirtualHost *:80>` block on line 1. Inside of that block, add the following block:

```
<Directory /var/www/html>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
</Directory>
```

Your file should now match the following. Make sure that all blocks are properly indented.

```
/etc/apache2/sites-available/default
<VirtualHost *:80>
    <Directory /var/www/html>

        . . .

    </Directory>

    . . .
</VirtualHost>
```

To put these changes into effect, restart Apache.

```
sudo service apache2 restart
```

Now, create the `.htaccess` file.

```
sudo nano /var/www/html/.htaccess
```

Add this first line at the top of the new file to activate the RewriteEngine.

```

RewriteEngine on
```

Save and exit the file.

To ensure that other users may only read your `.htaccess`, run the following command to update permissions.

```
sudo chmod 644 /var/www/html/.htaccess
```

You now have an operational `.htaccess` file, to govern your web application's routing rules.

+ Install MySQL on Ubuntu 16.04:

```
sudo apt-get update
sudo apt-get install mysql-server
mysql_secure_installation
```

+ user: root
+ password: admin1234

+ Testing MySQL: 

```
systemctl status mysql.service
```

If MySQL isn't running, you can start it with `sudo systemctl start mysql`


File `.htaccess` mẫu (mặc định cho một số trang):

```
RewriteEngine on
# basic compression
<IfModule mod_gzip.c>
	mod_gzip_on Yes
	mod_gzip_dechunk Yes
	mod_gzip_item_include file \.(html?|txt|css|js)$
	mod_gzip_item_include mime ^text/.*
	mod_gzip_item_include mime ^application/x-javascript.*
	mod_gzip_item_exclude mime ^image/.*
	mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</IfModule>

# Basic rewrite rules, stop unneeded PERL bot, block subversion directories
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteRule ^(.*/)?\.svn/ - [F,L]
	ErrorDocument 403 "Access Forbidden"
	RewriteCond %{HTTP_USER_AGENT} libwww-perl.*
	RewriteRule .* – [F,L]
</IfModule>

```

Nghiên cứu lại:

```
RewriteEngine on
# basic compression
<IfModule mod_gzip.c>
	mod_gzip_on Yes
	mod_gzip_dechunk Yes
	mod_gzip_item_include file \.(html?|txt|css|js)$
	mod_gzip_item_include mime ^text/.*
	mod_gzip_item_include mime ^application/x-javascript.*
	mod_gzip_item_exclude mime ^image/.*
	mod_gzip_item_exclude rspheader ^Content-Encoding:.*gzip.*
</IfModule>

# Protect files and directories
<FilesMatch "(\.(engine|inc|info|install|module|profile|po|sh|.*sql|theme|tpl(\.php)? |xtmpl)|code-style\.pl|Entries.*|Repository|Root|Tag|Template)$">
	Order allow,deny
</FilesMatch>

# Don’t show directory listings
Options -Indexes

# Basic rewrite rules, stop unneeded PERL bot, block subversion directories
<IfModule mod_rewrite.c>
	RewriteEngine On
	RewriteRule ^(.*/)?\.svn/ - [F,L]
	ErrorDocument 403 "Access Forbidden"
	RewriteCond %{HTTP_USER_AGENT} libwww-perl.*
	RewriteRule .* – [F,L]
</IfModule>
```


- Download the latest version of **Open Source Social Network** available at [http://www.opensource-socialnetwork.org/download](http://www.opensource-socialnetwork.org/download) to a directory on the server and extract it using the following commands:

```
cd /opt/
wget https://www.opensource-socialnetwork.org/download_ossn/latest/build.zip -O ossn.zip
unzip ossn.zip -d /var/www/html/
rm ossn.zip 
```

Create a new MySQL database and user for OSSN:

```
mysql -u root -p
mysql> SET GLOBAL sql_mode='';
mysql> CREATE DATABASE fullstacktechsocial;
mysql> CREATE USER 'ossnuser'@'localhost' IDENTIFIED BY 'admin123456';
mysql> GRANT ALL PRIVILEGES ON fullstacktechsocial.* TO 'ossnuser'@'localhost';
mysql> FLUSH PRIVILEGES;
mysql> quit
```

## Show users:

[http://www.mysqltutorial.org/mysql-show-users/](http://www.mysqltutorial.org/mysql-show-users/)
Secure Mysql: [https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-16-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-phpmyadmin-on-ubuntu-16-04)


Error: `Your password does not satisfy the current policy requirements`
Resolve: `mysql>uninstall plugin validate_password;`

Do not forget to replace ‘y0ur-pAssW0RD’ with a strong password.

Edit the PHP configuration file (/etc/php/7.0/cli/php.ini):

```
vi /etc/php/7.0/cli/php.ini
```

and modify these lines:

```
allow_url_fopen = On
file_uploads = On
upload_max_filesize = 32M
```

OSSN needs a directory for storing the uploaded files such as images. For security reasons we will create this directory outside of the document root directory:

```
mkdir -p /var/www/ossndatadir
```

All files have to be readable by the web server, so set a proper ownership:

```
chown www-data:www-data -R /var/www/html/ossn/
```

Create a new virtual host directive in Apache. For example, create a new Apache configuration file named ‘ossn.conf’ on your virtual server:

```
touch /etc/apache2/sites-available/ossn.conf
ln -s /etc/apache2/sites-available/ossn.conf /etc/apache2/sites-enabled/ossn.conf
vi /etc/apache2/sites-available/ossn.conf
```

Then, add the following lines:


```
<VirtualHost *:80>
    ServerAdmin admin@your-domain.com
    DocumentRoot /var/www/html/ossn/
    ServerName your-domain.com
    ServerAlias www.your-domain.com
<Directory /var/www/html/ossn/>
    Options FollowSymLinks
    AllowOverride All
    Order allow,deny
    allow from all
</Directory>
    ErrorLog /var/log/apache2/your-domain.com-error_log
    CustomLog /var/log/apache2/your-domain.com-access_log common
</VirtualHost>
```

Remove the 000-default.conf file:

```
rm /etc/apache2/sites-enabled/000-default.conf
```

Restart the Apache web server for the changes to take effect:

```
service apache2 restart
```

Nếu truy cập vào `/var/www/html` mà không thực thi được file **index.php** thì chạy 3 dòng lệnh sau đây:

```
sudo apt-get install php libapache2-mod-php
sudo a2enmod mpm_prefork && sudo a2enmod php7.0
sudo service apache2 restart
```

Open your favorite web browser, navigate to **http://your-domain.com/** , verify that all installation prerequisites are met and the OSSN installer should be starting. You should follow the easy instructions on the install screen inserting the necessary information as requested. Do not forget to set data directory to **‘/var/www/ossndatadir’**.

That is it. The Open Source Social Network has been installed on your server.

Log in to the OSSN administration back-end at **http://your-domain.com/administrator** and configure OSSN according to your needs.

**Note**: Xem phân quyền của các user mysql:

```
SHOW GRANTS  [FOR user]
```

Ex:

```
mysql> SHOW GRANTS FOR 'ossnuser'@'localhost';
```

```
mysql> SHOW GRANTS FOR 'jeffrey'@'localhost';
+------------------------------------------------------------------+
| Grants for jeffrey@localhost                                     |
+------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `jeffrey`@`localhost`                      |
| GRANT SELECT, INSERT, UPDATE ON `db1`.* TO `jeffrey`@`localhost` |
+------------------------------------------------------------------+
```

```
SHOW GRANTS;
SHOW GRANTS FOR CURRENT_USER;
SHOW GRANTS FOR CURRENT_USER();
```

Tài khoản admin:

+ User: HauNguyenTrung
+ Password: 0903024436


Một số ình ảnh cài đặt:

![](https://lh3.googleusercontent.com/fyTsrlg4W04PSwfCQvCueMUA0hUlngH1Z-fp1hcxeFI5tUGVG6UrjatPTzkvITrOqA_AWODoCgdgC98lU15Sf1Mraz_-zswqWeiXVBBE-l_jbH2Y8ekXUBzF9zhnxU7VOzBWGIW1O9xLgXBmqJBzr95h0RDcs2cZzVi6nXy4MT2RyY97kjYq5Q77oD_kUFADxOXN1Z49AJteV4Q-XnNImX4SIvVeWZAXCpKeBv6pGV7OFGwTUFWSMqBDeA1GSUX-DU9UmPLDVkYbOsvSWMBpKF5jrkYWdYywoa705ENA2JGwxvzz6F7vUY4zqHQOnEhNluKGjKrOOtCKsTcHZoQw6UjmozLiLhatQ6GfSt3hRXaPSi6HoeD8PQjtNkL7F-eH_Bo9bLKEKH5uJKXuczqjIgLjeI_54lElUoARdJQWwix7CQI2uG70x9q2XgbAtFu8N1OFFVmDANzoM9ruBACOPOze8x4LWzAM-6azqs0IZisB5-E_tLwImgfUWzlut5qfrmaBx-Gi53SQh1p8ZKXJuRp1xbVcXZ-nA7ci4ypKfVeBZx7BEtwt5Mz1p_dQS5yQiJDS0u0sy4-WQGjOIYYPEqpM_9g9KIAgZqysx_M3nhOh0IY73PebOPtcCkNkbz9cXL7hiohipiWjTQkxm6HwDtF_Hv9LTjQ=w1073-h493-no)

![](https://lh3.googleusercontent.com/8zCRls9SUrrJbL-kBbNPxLo4UjEoQLuZs-99ywU77ZVXKOhrW0HgsEWLdCIFyXxnQFHdtxKRgswsIi9lgY4KEVmhG2iR9LtPgdiNr34Mnl5vwNY99Tzog0gc26sJEYN02xIr3NC8fGBPIw31UamzpcJvbWo0gV6NQx4f5a8YHJ0QmbhSJhxa6laC3KRFSmKRZzrBo0R6i94V5EkuttuquioefCS-_wHgSBnBJi-3Ey1xgcePYDDUiZuEDxjCexz5yMcKKeDls5rako_-sMeBa837o8VkYZAtQ-O1mIw7gUqDnxLOhadeUuz-Quv2wcvCbrgv9MnF5Vl2-YjAw8iOwRfMFUJeqAZS1D1zShdDssFnJWUzRWUXtfBTH87fp-v9nsvYakgsXiP5m6h_-HXwwRzF3Ou4zmmDb4jGLDjfAlcoO-Lz-1uh9L09fwEzF5yMpv0VKKqv6cLOTQ8K9m5sRtHmFYCNh6LqpwsddDx3_Kb9DEfnRlc1jEQKvTaSGQeCr257yJHx2Miq6XAV7anY94X29kjjFG0ykK-wkvQuzFhX1Kt-rveE-iWihOhhAdk069WJ5Qai7cLztzuAY_GSoAOs5ykeCUJuCO-pakZ9hKszLY3IBTlQ6HPgCueBjZGTewNbTmrXGDotyPfV-b_uKvGN_RyPayA=w1031-h407-no)